package com.orsyp.migration.univAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.calendar.Calendar;
import com.orsyp.api.calendar.CalendarFilter;
import com.orsyp.api.calendar.CalendarId;
import com.orsyp.api.calendar.CalendarItem;
import com.orsyp.api.calendar.CalendarList;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.networknode.NetworkNodeId;
import com.orsyp.api.central.services.INetworkNodeService;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobFilter;
import com.orsyp.api.job.JobItem;
import com.orsyp.api.job.JobList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.user.User;
import com.orsyp.central.jpa.NetworkNodeTypeEnum;
import com.orsyp.central.jpa.jpo.HostEntity;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.std.CalendarListStdImpl;
import com.orsyp.std.CalendarStdImpl;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.JobListStdImpl;
import com.orsyp.std.JobStdImpl;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.std.UserStdImpl;
import com.orsyp.std.central.UniCentralStdImpl;
import com.orsyp.util.ApplicationVariables;

public class UJConnection {
	
	private UniCentral central;
	private String user;
	private String host;
	private Map<String, String[]> nodes = null;
	private HashMap<String, Context> contexts = new HashMap<String, Context>();
	
	// private constructor, force to use factory method
	private UJConnection() {
	}
	
	public static UJConnection createUniJobConnection(String host, int port, String user, String pw)  throws Exception {
		UJConnection conn = new UJConnection();
		conn.central = new UniCentral(host, port);
		conn.central.setImplementation(new UniCentralStdImpl(conn.central));
		conn.central.setSslEnabled(ApplicationVariables.SSL_SECURITY.equals(System.getProperty(ApplicationVariables.SECURITY_TYPE)));
		conn.host = host;
		conn.user = user;
		// login
		try {
			conn.central.login(user, pw);
		} catch (UniverseException e) {
			e.printStackTrace();
			throw new Exception("Login failed");
		}

		ConnectionFactory factory = NodeConnectionFactory.getInstance(conn.central);
		ClientConnectionManager.setDefaultFactory(factory);
		return conn;
	}	

	public Context getContext(String node) throws Exception {
		if (!contexts.containsKey(node))
			contexts.put(node, makeContext(node));
		return contexts.get(node);
	}
	
	public Context makeContext(String node) throws SyntaxException {
		Context context = null;

		if (nodes == null)
			nodes = getNodes();

		Client client = new Client(new Identity(user, "", nodes.get(node)[0], ""));
		context = new Context(new Environment("UNIV64", node), client, central);
		context.setProduct(Product.UNIJOB);
		return context;
	}	
	
	public Map<String, String[]> getNodes() {		
		Client client = new Client(new Identity(user, "", host, ""));

		ConnectionFactory factory = NodeConnectionFactory.getInstance(central);
		ClientConnectionManager.setDefaultFactory(factory);

		Environment envCentral;
		try {
			envCentral = new Environment("UJCENT", "UJCENT");
			envCentral.setNodeId("UJCENT");
		} catch (SyntaxException e) {
			System.err.println("Syntax error for environment: " + e.getMessage());
			return null;
		}

		Context ctx = new Context(envCentral, client, central);
		ctx.setProduct(Product.UNICENTRAL);

		ClientServiceLocator.setContext(ctx);
		INetworkNodeService nnService = ClientServiceLocator.getNetworkNodeService();
		List<NetworkNodeEntity> l = nnService.getNodesByTypeAndWildcard(NetworkNodeId.PRODUCT_TYPE.UNIJOB, "*", "*");

		TreeMap<String, String[]> map = new TreeMap<String, String[]>(String.CASE_INSENSITIVE_ORDER);

		for (NetworkNodeEntity entity : l) 
			if (entity.getProductType()==NetworkNodeTypeEnum.UNIJOB) {
				Set<HostEntity> hosts = entity.getHosts();
				HostEntity host = hosts.iterator().next();
				if (host != null)
					map.put(entity.getName(), new String[] {host.getName(), "WIN"}); //TODO temp, read real os
			}
		
		return map;
	}

	//single node methods
	
	public List<JobItem> getJobs (String node) throws Exception {		
		JobList l = new JobList(getContext(node));
		l.setImpl(new JobListStdImpl());
		l.setSyntaxRules(ClassicSyntaxRules.getInstance());
		l.setFilter(new JobFilter());
		l.extract();
		
		ArrayList<JobItem> res = new ArrayList<JobItem>();  
		for (int i = 0; i<l.getCount(); i++) 			
			res.add(l.get(i));
		
		return res;
	}
	
	public Job getJob (JobItem item) throws Exception {		
		Job j = new Job(item);
		j.setImpl(new JobStdImpl());		
		j.extract();
		return j;
	}

	public User getUniJobUser(String userName, String node) throws Exception {		
		User u = new User(getContext(node), userName);
		u.setImpl(new UserStdImpl());
		u.extract();
		return u;
	}

	public HashMap<String,Calendar> getCalendars(String node) throws Exception {
		CalendarList l = new CalendarList(getContext(node), new CalendarFilter());
		l.setImpl(new CalendarListStdImpl());
		l.setSyntaxRules(OwlsSyntaxRules.getInstance());		
		l.extract();
		
		HashMap<String,Calendar> cals = new HashMap<String,Calendar>();
		
		for (int i=0;i<l.getCount();i++) {
			CalendarItem item = l.get(i);
			Calendar c = new Calendar(item);
			c.setImpl(new CalendarStdImpl());
			c.extract();
			cals.put(item.getIdentifier().toString(),c);
		}
		return cals;		
	}
	
	/*
	public List<Job> getTargets (String node) throws Exception {		
		Target l = new JobList(getContext(node));
		l.setImpl(new JobListStdImpl());
		l.setSyntaxRules(ClassicSyntaxRules.getInstance());
		l.setFilter(new JobFilter());
		l.extract();
		
		ArrayList<JobItem> res = new ArrayList<JobItem>();  
		for (int i = 0; i<l.getCount(); i++) 			
			res.add(l.get(i));
		return res;
	}
	*/
	
}
