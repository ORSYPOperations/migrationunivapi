/**
 * 
 */
package com.orsyp.migration.univAPI.unijob;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.FunctionalPeriod;
import com.orsyp.api.InvalidDataException;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobStatus;
import com.orsyp.api.rule.KmeleonPattern;
import com.orsyp.api.rule.Offset;
import com.orsyp.api.rule.Pattern;
import com.orsyp.api.rule.PositionsInPeriod;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.rule.UniPattern;
import com.orsyp.api.rule.UniPattern.RunOverEnum;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.task.DayType;
import com.orsyp.api.task.SimpleLaunch;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskCyclicData;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskImplicitData;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.api.task.TaskSpecificData;
import com.orsyp.api.task.TaskType;
import com.orsyp.owls.impl.task.OwlsTaskImpl;

/**
 * @author rbj
 *
 */
public class TaskConverter {
    
    private static final String DEFAULT_PRINTER = "";
    private static final String DEFAULT_PRIORITY = "100";

    public static Task convert(Job job, String newName, String mu, Context context) throws UniverseException {
        
    	String ver =createVersion(context);
    	
    	Task task = new Task(context, TaskId.createWithName(newName, ver, mu.toUpperCase(), false));
    	task.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
    	task.setImpl(new OwlsTaskImpl());
        task.setLabel("Converted from UniJob");
        task.setTaskType(TaskType.Scheduled);
        
        task.getIdentifier().setUprocName(newName);
        task.getIdentifier().setUprocVersion(ver);
        
        // Set the begin of validity period as the reference date
        task.setValidFrom(job.getValidFrom());
        // Valid To
        task.setValidTo(job.getValidTo());
        
        
        task.setUserName(job.getUserName());        
        //mu
        task.setMuName(mu.toUpperCase());
                
        task.setFunctionalPeriod(FunctionalPeriod.Day);
                
        task.setUnitOffset(0);
        task.setDayOffset(0);
        
        task.setQueue("SYS_BATCH");
        task.setPriority("100");
        task.setPrinter("PRN");
                        
        if (job.getStatus() == JobStatus.DISABLED)
            task.setActive(false);
        else if( job.getStatus() == JobStatus.ENABLED)
            task.setActive(true);
        else {
            task.setActive(true);
            task.setSimulated(true);
        }
        
        task.setAutoRestart(false);
        task.setCentral(false);
        task.setInteractiveFlag(job.isInteractive());
        task.setEndExecutionForced(false);
        task.setTypeDayOffset(DayType.CALENDAR);
        task.setParallelLaunch(true);
                
        createTaskSpecificData(task,job);
        
        /*
        TaskId taskId = TaskId.createWithName("", "", newName,createVersion(context), mu, false);
        taskId.setSyntaxRules(OwlsSyntaxRules.getInstance());
        taskId.setName(newName);
        
        Task task = new Task(context, taskId);
        task.setImpl(new OwlsTaskImpl());
        
        //Set context
        task.setContext(context);
        
       
        // Status
        if (job.getStatus() == JobStatus.DISABLED){
            task.setActive(false);
        } else if( job.getStatus() == JobStatus.ENABLED){
            task.setActive(true);
        } else {
            task.setActive(true);
            task.setSimulated(true);
        }
        
        
        // Set the begin of validity period as the reference date
        task.setValidFrom(job.getValidFrom());
        // Valid To
        task.setValidTo(job.getValidTo());
        
        
        task.setParallelLaunch(true);        
        task.setUserName(job.getUserName());
        task.setQueue("SYS_BATCH");
        task.setPrinter(DEFAULT_PRINTER);
        task.setPriority(DEFAULT_PRIORITY);
        task.setAutoRestart(false);
        task.setCentral(false);
        task.setInteractiveFlag(job.isInteractive());
        
        task.setDayOffset(0);
        
        task.setEndExecutionForced(false);
        task.setTypeDayOffset(DayType.CALENDAR);        
        
        //planification data
        createTaskSpecificData(task,job);
        */
        
        return task;
    }

    /**
     * Creates a new <code>TaskSpecificData</code> from the <code>Job</code> to
     * be converted.
     * @param task 
     * 
     * @param job
     *            - job to be converted
     * @return new <code>TaskSpecificData</code>
     * @throws InvalidDataException 
     */
    private static void createTaskSpecificData(Task task, Job job) throws InvalidDataException {
        TaskSpecificData planification = job.getPlanification();
        if (planification instanceof TaskCyclicData) {
            task.setTaskType(TaskType.Cyclic);
            TaskCyclicData tcd = ((TaskCyclicData) planification).clone();
            task.setSpecificData(tcd);
        } else if (planification instanceof TaskPlanifiedData) {
            task.setTaskType(TaskType.Scheduled);
            TaskPlanifiedData tpd =((TaskPlanifiedData)planification).clone();
            TaskImplicitData[] implicitData = tpd.getImplicitData();
            parseImplicitData(implicitData);
            task.setSpecificData(tpd);
        }
    }

    /**
     * @param implicitData
     * @throws InvalidDataException 
     */
    private static void parseImplicitData(TaskImplicitData[] implicitData) throws InvalidDataException {
        for (TaskImplicitData tid : implicitData) {
            Pattern pattern = tid.getPattern();
            if(pattern instanceof KmeleonPattern){
                KmeleonPattern kPat = (KmeleonPattern) pattern;
                UniPattern uniPat = new UniPattern();
                
                PositionsInPeriod kPositions = kPat.getPositionsInPeriod();
                PositionsInPeriod positions = new PositionsInPeriod();
                positions.setForward(kPositions.isForward());
                positions.setPositionsPattern(kPositions.getPositionsPattern());
                positions.setType(kPositions.getType());
                
                uniPat.setPositionsInPeriod(positions);
                
                SimpleLaunch kSimpleLaunch = kPat.getSimpleLaunch();
                if(kSimpleLaunch != null){
                    SimpleLaunch simpleLaunch = new SimpleLaunch();
                    if(kSimpleLaunch.getHoursCount() != null){
                        simpleLaunch.setHoursCount(kSimpleLaunch.getHoursCount());
                    }else{
                        simpleLaunch.setHoursCount("0");
                    }
                    if(kSimpleLaunch.getMinutesCount() != null){
                        simpleLaunch.setMinutesCount(kSimpleLaunch.getMinutesCount());
                    }else{
                        simpleLaunch.setMinutesCount("10");
                    }
                    if(kSimpleLaunch.getSecondsCount() != null){
                        simpleLaunch.setSecondsCount(kSimpleLaunch.getSecondsCount());
                    }else{
                        simpleLaunch.setSecondsCount("0");
                    }
                    simpleLaunch.setLaunchTime(kSimpleLaunch.getLaunchTime());
                    tid.setEmbeddedLaunchTime(simpleLaunch);
                }
                
                uniPat.setPositionDirection(Rule.Direction.FROM_BEGINNING);
                uniPat.setDayAuthorizations(kPat.getDayAuthorizations());
                uniPat.setRunOver(kPat.isRunOver()?RunOverEnum.NO:RunOverEnum.YES);
                uniPat.setOffset(new Offset());
                
                tid.setPattern(uniPat);
            }
        }
    }

    /**
     * Builds the Uproc version of the DUAS Uproc of this implementation
     * according to the target DUAS/TEAM area
     * 
     * @return the uproc version
     */
    protected static String createVersion(Context context) {
        String version;
        String area = context.getEnvironment().getArea().toString();
        if ((area.compareTo("X") == 0) || (area.compareTo("S") == 0)){
            version = "000";
        } else{
            version = "001";
        }
        return version;
    }    
}
