/**
 * 
 */
package com.orsyp.migration.univAPI.unijob;

import java.util.Vector;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.FunctionalPeriod;
import com.orsyp.api.ObjectList;
import com.orsyp.api.SyntaxRules.Provider;
import com.orsyp.api.Variable;
import com.orsyp.api.VariableJobRequired;
import com.orsyp.api.VariableText;
import com.orsyp.api.job.Job;
import com.orsyp.api.shell.Shell;
import com.orsyp.api.uproc.CompletionInstruction;
import com.orsyp.api.uproc.Incompatibility;
import com.orsyp.api.uproc.LaunchFormula;
import com.orsyp.api.uproc.Memorization;
import com.orsyp.api.uproc.ReturnCodePattern;
import com.orsyp.api.uproc.StatusManagement;
import com.orsyp.api.uproc.Successor;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocId;
import com.orsyp.api.uproc.cmd.CmdData;
import com.orsyp.owls.impl.uproc.OwlsUprocImpl;

/**
 * @author rbj
 *
 */
public class UprocConverter {
    
    private static final String SHELL_ARGS = "/c \"!CMD!\"";
    private static final String SHELL_PATH = "cmd";
    private static final String SHELL_NAME = "UNIJOB_SHELL";
    private static final String application = "U_";
    private static final String domain = "I";
    
    public static Uproc convert(Job job, String newName, Context context) throws UniverseException {
        
        UprocId uprocId = new UprocId(newName, createVersion(context));
        uprocId.setSyntaxRules(Provider.getSyntaxRules(context.getProduct()));
        
        Uproc uproc = new Uproc(context, uprocId);
        uproc.setImpl(new OwlsUprocImpl());
        uproc.setCompletionInstructions(new Vector<CompletionInstruction>());
        uproc.setSuccessors(new Vector<Successor>());
        uproc.setDefaultInformation(job.getDefaultInformation());
        
        Vector<Variable> variables = new Vector<Variable>();
        for (VariableJobRequired variable:job.getVariableList()) {
            VariableText uprVar = new VariableText();
            uprVar.setLength(255);
            uprVar.setName(variable.getName());
            uprVar.setOrigin(variable.getOrigin());
            uprVar.setValue(variable.getValue());
            variables.add(uprVar);
        }
        
        uproc.setVariables(variables);
        
        uproc.setFormula(new LaunchFormula());
        uproc.setApplication(application);
        uproc.setDomain(domain);
        uproc.setIncompatibilities(new Vector<Incompatibility>());
        StatusManagement statusManagement = createStatusManagement(job);
        uproc.setStatusManagement(statusManagement);
        uproc.setInteractive(job.isInteractive());
        uproc.setUprocClass("");
        uproc.setFunctionalPeriod(FunctionalPeriod.Day);
        uproc.setMemorization(new Memorization(Memorization.Type.ONE));
        uproc.setType("CMD");
        uproc.setSpecificData(createCmdData(job));
        
        uproc.setAddList(job.getAddList());
        uproc.setDefaultSeverity(job.getDefaultSeverity());
        uproc.setEmailNotification(job.getEmailNotification());
        if(job.getIcon() != null){
            uproc.setIcon(job.getIcon());
        }
        
        return uproc;
    }
    
    /**
     * Builds the Uproc version of the DUAS Uproc of this implementation
     * according to the target DUAS/TEAM area
     * 
     * @return the uproc version
     */
    protected static String createVersion(Context context) {
        String version;
        String area = context.getEnvironment().getArea().toString();
        if ((area.compareTo("X") == 0) || (area.compareTo("S") == 0)){
            version = "000";
        } else{
            version = "001";
        }
        return version;
    }
    
    /**
     * Creates a fake, but valid, StatusManagement object
     * @param job 
     * 
     * @return a StatusManagement object with some default values
     */
    protected static StatusManagement createStatusManagement(Job job) {
        StatusManagement jobSm = job.getStatusManagement();
        StatusManagement sm = new StatusManagement();        
        if(jobSm != null){
            sm.setStatus(jobSm.getStatus());
            sm.setType(jobSm.getType());
            sm.setReturnCode(jobSm.getReturnCode());
            sm.setEnabled(jobSm.isEnabled());
            sm.setOperator(jobSm.getOperator());
            sm.setSearchString(jobSm.getSearchString());
            sm.setFilename(jobSm.getFilename());
            sm.setFileType(jobSm.getFileType());
            if (jobSm.getPattern()!=null)
            	sm.setPattern(new ReturnCodePattern(jobSm.getPattern().getPattern()));
            else
            	sm.setPattern(new ReturnCodePattern(""));
        } else {
            sm.setStatus(StatusManagement.Status.COMPLETED);
            sm.setType(StatusManagement.Type.RETURN_CODE);
            sm.setReturnCode(0);
            sm.setEnabled(false);
            sm.setOperator(StatusManagement.Operator.EQUAL);
            sm.setSearchString("");
            sm.setFilename("");
            sm.setFileType(StatusManagement.FileType.JOB_LOG);
            sm.setPattern(new ReturnCodePattern(""));
        }
        return sm;
    }
    
    /**
     * Builds the command data of the Uproc of this implementation
     * 
     * @return the command data of the Uproc
     * @throws UniverseException
     */
    protected static CmdData createCmdData(Job job) throws UniverseException {
        Context context = job.getContext();
        
        CmdData cmdData = new CmdData();
        cmdData.setExecutionDirectory(job.getExecutionDirectory());
        cmdData.setCommandLine(job.getCommand());
        
        Shell shell = null;
        if(job.getShell() != null){
            ObjectList<Shell> shells = new ObjectList<Shell>(context);
            //shells.setImpl(new OwlsImplFactory().getShellObjectListImpl()); 
            //TODO OwlsImplementationProvider.getObjectListImplFactory().getShellObjectListImpl()     
            shells.extract();
            for (int i=0; i<shells.getCount(); i++) {
                if(shells.get(i).getName().equals(job.getShell())){
                    shell = shells.get(i);
                }
            }
        }
        
        if(shell != null){
            // create shell only for UniJob
            cmdData.setShell(job.getShell());
            cmdData.setShellArgments(shell.getArgument());
            cmdData.setShellPath(shell.getPath());
        }else{
            if(job.getShell() != null){
                cmdData.setShell(job.getShell());
            }else{
                cmdData.setShell(SHELL_NAME);
            }
            cmdData.setShellArgments(SHELL_ARGS);
            cmdData.setShellArgments(SHELL_PATH);
        }
        
        return (cmdData);
    }

}
