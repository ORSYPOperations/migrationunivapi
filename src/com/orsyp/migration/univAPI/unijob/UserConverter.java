package com.orsyp.migration.univAPI.unijob;

import com.orsyp.SyntaxException;
import com.orsyp.api.Context;
import com.orsyp.api.ObjectAlreadyExistException;
import com.orsyp.api.Product;
import com.orsyp.api.SyntaxRules.Provider;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserSystem;
import com.orsyp.api.user.UserSystem.Type;
import com.orsyp.api.user.UserType;
import com.orsyp.log.Log;
import com.orsyp.log.LogFactory;
import com.orsyp.owls.impl.user.OwlsUserImpl;

public class UserConverter {
	private static final Log log = LogFactory.getLog(UserConverter.class.getName());	

	
	/*
			
			User user = createUser(userName);
			try {
                user.setName(userName);
            } catch (SyntaxException e) {
                log.error(e);
            }
			
			//check the special case of a local system
			if(userName.equals("LocalSystem")){
				user.getUserSystem().setType(Type.W32);
				user.getUserSystem().setUser(userName);
			}
			else{
			    user.getUserSystem().setType(Type.OTHER);
                user.getUserSystem().setUser(userName);
			}
			
			object.setUser(user);
			
			if(shouldUpdatePassword(object)){
				users.add(user);
			}
		}
		return users;
	}
	*/
	
	public static User createUser(String name, String pw, Context c) throws Exception{
        UserId userId = UserId.create(name);
        userId.setSyntaxRules(Provider.getSyntaxRules(Product.OWLS));
        User user = new User(userId);
        user.setContext(c);
        user.setImpl(new OwlsUserImpl());
        UserSystem userSystem = new UserSystem();        
        userSystem.setUser(name);
        userSystem.setPsw(pw);
        if(name.equals("LocalSystem"))
        	userSystem.setType(Type.W32);			
		else
			userSystem.setType(Type.OTHER);                
        user.setUserSystem(userSystem);
        
        try{
            if (isRoot(name)) 
                user.setProfile("ADMIN");
            else 
                user.setProfile("UNIJOB");
        } catch (SyntaxException e) {
            log.error(e);
        }
        user.setUserType(UserType.Both);
    
        try {
	        if (name.length()>3) { 
	        	user.setAuthorCode(name.substring(0, 3).toUpperCase());
	        	user.create();
	        	return user;
	        }
        }
        catch (Exception e) {}
        
        int idx=1;
        while (true) 
	        try {
		        user.setAuthorCode(String.format("%03d", idx));
		        user.create();
	        	return user;
	        }
        	catch (ObjectAlreadyExistException e) {
        		return user;
        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	idx++;	        	
	        }
	}
	
	/**
     * @param user
     * @return
     */
    private boolean invalidUserSystem(User user) {
        boolean isLocalSystem = "LocalSystem".equals(user.getUserSystem().getUser());
        boolean hasEmptyPassword = user.getUserSystem().getPsw() == null || user.getUserSystem().getPsw().isEmpty();
        return !isLocalSystem && hasEmptyPassword;
    }

    private static boolean isRoot(String user) {
		if ( (user == null) || (user.length()==0) )
			return false;
		return user.compareTo("root") == 0;
	}

}
