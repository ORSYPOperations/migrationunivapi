package com.orsyp.migration.univAPI;

import java.awt.Color;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.FunctionalPeriod;
import com.orsyp.api.InvalidMuException;
import com.orsyp.api.Product;
import com.orsyp.api.Timezone;
import com.orsyp.api.Variable;
import com.orsyp.api.VariableDate;
import com.orsyp.api.VariableNumeric;
import com.orsyp.api.VariableText;
import com.orsyp.api.application.Application;
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.bv.BusinessViewId;
import com.orsyp.api.bv.BvmCriteria;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.services.INetworkNodeService;
import com.orsyp.api.execution.ExecutionFilter;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.execution.ExecutionList;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobDuration;
import com.orsyp.api.job.JobDuration.DurationType;
import com.orsyp.api.job.JobDuration.KActionType;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuFilter;
import com.orsyp.api.mu.MuItem;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.mu.TargetVariable;
import com.orsyp.api.mu.TargetVariableId;
import com.orsyp.api.node.NodeVariable;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.resource.ResourceId;
import com.orsyp.api.resource.SpecificFile;
import com.orsyp.api.resource.SpecificLogical;
import com.orsyp.api.resource.SpecificSystem;
import com.orsyp.api.rule.KDayAuthorization;
import com.orsyp.api.rule.KDayAuthorization.KDayAuthorizationType;
import com.orsyp.api.rule.KmeleonPattern.KDayType;
import com.orsyp.api.rule.MonthAuthorization;
import com.orsyp.api.rule.MonthAuthorization.Direction;
import com.orsyp.api.rule.Offset;
import com.orsyp.api.rule.PositionsInPeriod;
import com.orsyp.api.rule.PositionsInPeriod.SubPeriodType;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.rule.Rule.PeriodTypeEnum;
import com.orsyp.api.rule.RuleFilter;
import com.orsyp.api.rule.RuleId;
import com.orsyp.api.rule.RuleItem;
import com.orsyp.api.rule.RuleList;
import com.orsyp.api.rule.UniPattern;
import com.orsyp.api.rule.UniPattern.RunOverEnum;
import com.orsyp.api.rule.WeekAuthorization;
import com.orsyp.api.rule.YearAuthorization;
import com.orsyp.api.security.Operation;
import com.orsyp.api.session.ExecutionContext;
import com.orsyp.api.session.Session;
import com.orsyp.api.session.SessionAtom;
import com.orsyp.api.session.SessionData;
import com.orsyp.api.session.SessionId;
import com.orsyp.api.session.SessionTree;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.task.DetailedSimulation;
import com.orsyp.api.task.ExclusionDate;
import com.orsyp.api.task.ExplicitLaunchDate;
import com.orsyp.api.task.LaunchHourPattern;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskImplicitData;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.api.task.TaskProvokedData;
import com.orsyp.api.task.TaskType;
import com.orsyp.api.uproc.AutoRetry;
import com.orsyp.api.uproc.CompletionInstruction;
import com.orsyp.api.uproc.DependencyCondition;
import com.orsyp.api.uproc.DependencyCondition.Status;
import com.orsyp.api.uproc.EmailNotification;
import com.orsyp.api.uproc.LaunchFormula;
import com.orsyp.api.uproc.Memorization;
import com.orsyp.api.uproc.Memorization.Type;
import com.orsyp.api.uproc.MuControl;
import com.orsyp.api.uproc.NonSimultaneityCondition;
import com.orsyp.api.uproc.ProcessingDateControl;
import com.orsyp.api.uproc.ProcessingDateControl.CalendarUnit;
import com.orsyp.api.uproc.ProcessingDateControl.DayType;
import com.orsyp.api.uproc.ResourceCondition;
import com.orsyp.api.uproc.ResourceCondition.Operator;
import com.orsyp.api.uproc.ReturnCodePattern;
import com.orsyp.api.uproc.SessionControl;
import com.orsyp.api.uproc.StatusManagement;
import com.orsyp.api.uproc.UniverseCondition;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocFilter;
import com.orsyp.api.uproc.UprocId;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.api.uproc.UserControl;
import com.orsyp.api.uproc.cl.ExternalScript;
import com.orsyp.api.uproc.cl.InternalScript;
import com.orsyp.api.uproc.cmd.CmdData;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserFilter;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserItem;
import com.orsyp.api.user.UserList;
import com.orsyp.api.user.UserSystem;
import com.orsyp.api.user.UserType;
import com.orsyp.central.jpa.jpo.AreaEntity;
import com.orsyp.central.jpa.jpo.HostEntity;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.migration.datamodel.common.CheckType;
import com.orsyp.migration.datamodel.common.TimeFrame;
import com.orsyp.migration.datamodel.common.DuVariable;
import com.orsyp.migration.datamodel.common.commonlists.DuMu;
import com.orsyp.migration.datamodel.common.commonlists.DuMu.OSType;
import com.orsyp.migration.datamodel.common.commonlists.DuRule;
import com.orsyp.migration.datamodel.common.commonlists.DuUser;
import com.orsyp.migration.datamodel.common.commonlists.rules.DayOfWeek;
import com.orsyp.migration.datamodel.common.commonlists.rules.Period;
import com.orsyp.migration.datamodel.common.commonlists.rules.Period.PeriodType;
import com.orsyp.migration.datamodel.common.commonlists.rules.Period.PositionType;
import com.orsyp.migration.datamodel.common.templates.TemplateTable;
import com.orsyp.migration.datamodel.du.BaseDuObject;
import com.orsyp.migration.datamodel.du.Condition;
import com.orsyp.migration.datamodel.du.Condition.UProcStatus;
import com.orsyp.migration.datamodel.du.ConditionChecks;
import com.orsyp.migration.datamodel.du.DUDataModel;
import com.orsyp.migration.datamodel.du.DurationControl;
import com.orsyp.migration.datamodel.du.SessionTreeElement;
import com.orsyp.migration.datamodel.du.TerminationInstruction;
import com.orsyp.migration.datamodel.du.UProc;
import com.orsyp.migration.datamodel.du.UProc.UProcType;
import com.orsyp.migration.datamodel.generic.GenericDataModel;
import com.orsyp.migration.univAPI.unijob.TaskConverter;
import com.orsyp.migration.univAPI.unijob.UprocConverter;
import com.orsyp.migration.univAPI.unijob.UserConverter;
import com.orsyp.migration.utils.FileUtils;
import com.orsyp.migration.utils.TemplateUtils;
import com.orsyp.owls.impl.application.OwlsApplicationImpl;
import com.orsyp.owls.impl.bv.OwlsBusinessViewImpl;
import com.orsyp.owls.impl.execution.OwlsExecutionListImpl;
import com.orsyp.owls.impl.mu.OwlsMuImpl;
import com.orsyp.owls.impl.mu.OwlsMuListImpl;
import com.orsyp.owls.impl.resource.OwlsResourceImpl;
import com.orsyp.owls.impl.rule.OwlsRuleImpl;
import com.orsyp.owls.impl.rule.OwlsRuleListImpl;
import com.orsyp.owls.impl.session.OwlsSessionImpl;
import com.orsyp.owls.impl.task.OwlsTaskImpl;
import com.orsyp.owls.impl.uproc.OwlsUprocImpl;
import com.orsyp.owls.impl.uproc.OwlsUprocListImpl;
import com.orsyp.owls.impl.user.OwlsUserImpl;
import com.orsyp.owls.impl.user.OwlsUserListImpl;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.std.NodeVariableStdImpl;
import com.orsyp.std.TargetVariableStdImpl;
import com.orsyp.std.central.UniCentralStdImpl;
import com.orsyp.util.ApplicationVariables;
import com.orsyp.util.DateTools;
import com.orsyp.util.io.Lines;


/**
 * UVMS connection wrapper.
 * $U API wrapper
 * @author rbr
 *
 */
public class UVMSConnection {

	private UniCentral central;
	private String user;
	private String host;
	private String company;
	private String area;
	private String referenceNode;
	
	private String sysUser;
	private String sysPassword;
	private com.orsyp.api.user.UserSystem.Type sysType = UserSystem.Type.W32;
	
	private String defaultVersion = "001";

	private HashMap<String, String> nodes = null;
	private HashMap<String, Context> contexts = new HashMap<String, Context>();
	private HashMap<String, String> appDomains = new HashMap<String, String>();

	// private constructor, force to use factory method
	private UVMSConnection() {
	}

	/**
	 * Connect to $U
	 * @param host
	 * @param port
	 * @param user
	 * @param pw
	 * @param company
	 * @param area
	 * @param referenceNode
	 * @param sysUser
	 * @param sysPassword
	 * @param systemOs
	 * @return
	 * @throws Exception
	 */
	public static UVMSConnection createConnection(String host, int port,
													String user, String pw, String company, String area,
													String referenceNode, String sysUser, String sysPassword, String systemOs) throws Exception {
		UVMSConnection conn = new UVMSConnection();
		conn.central = new UniCentral(host, port);
		conn.central.setImplementation(new UniCentralStdImpl(conn.central));
		conn.central.setSslEnabled(ApplicationVariables.SSL_SECURITY.equals(System.getProperty(ApplicationVariables.SECURITY_TYPE)));
		conn.host = host;
		conn.user = user;
		conn.company = company;
		conn.area = area;
		conn.referenceNode = referenceNode;
		conn.sysUser = sysUser;
		conn.sysPassword = sysPassword;				
		conn.sysType = com.orsyp.api.user.UserSystem.Type.W32;
		if (!systemOs.equals("W32"))
			conn.sysType = com.orsyp.api.user.UserSystem.Type.OTHER;
		if (Arrays.asList("A", "APP", "I", "INT").contains(area.toUpperCase())) 
			conn.defaultVersion = "001";
		else
			conn.defaultVersion = "000";

		// login
		try {
			conn.central.login(user, pw);
		} catch (UniverseException e) {
			e.printStackTrace();
			throw new Exception("Login failed");
		}

		ConnectionFactory factory = NodeConnectionFactory.getInstance(conn.central);
		ClientConnectionManager.setDefaultFactory(factory);
		return conn;
	}

	public static void cleanup() {
		ClientConnectionManager.cleanup();
	}

	public Context makeContext(String node, String targetArea) throws SyntaxException {
		Context context = null;

		Area areaObj = Area.Exploitation;
		if (Arrays.asList("A", "APP").contains(targetArea.toUpperCase())) 
			areaObj = Area.Application;
		else if (Arrays.asList("I", "INT").contains(targetArea.toUpperCase())) 
			areaObj = Area.Integration;
		else if (Arrays.asList("S", "SIM").contains(targetArea.toUpperCase())) 
			areaObj = Area.Simulation;

		if (nodes == null)
			nodes = getNodeList();

		Client client = new Client(new Identity(user, "", nodes.get(node), ""));
		context = new Context(new Environment(company, node, areaObj), client, central);
		context.setProduct(Product.OWLS);

		return context;
	}	

	public Context getContext(String node) throws Exception {
		if (!contexts.containsKey(node))
			contexts.put(node, makeContext(node,area));
		return contexts.get(node);
	}

	public Context getContext() throws Exception {
		return getContext(referenceNode);
	}

	public HashMap<String, String> getNodeList() {
		return getNodeList(null,null);
	}
	
	public HashMap<String, String> getNodeList(String companyFilter, String areaFilter) {		
		Client client = new Client(new Identity(user, "", host, ""));

		ConnectionFactory factory = NodeConnectionFactory.getInstance(central);
		ClientConnectionManager.setDefaultFactory(factory);

		Environment envCentral;
		try {
			envCentral = new Environment("UJCENT", "UJCENT");
			envCentral.setNodeId("UJCENT");
		} catch (SyntaxException e) {
			System.err.println("Syntax error for environment: " + e.getMessage());
			return null;
		}

		Context ctx = new Context(envCentral, client, central);
		ctx.setProduct(Product.UNICENTRAL);

		ClientServiceLocator.setContext(ctx);
		INetworkNodeService nnService = ClientServiceLocator.getNetworkNodeService();
		List<NetworkNodeEntity> nnes = new ArrayList<NetworkNodeEntity>();
		nnes.addAll(nnService.getOwlsNodesForCompanyAndNode("*", "*"));

		HashMap<String, String> map = new HashMap<String, String>();

		for (NetworkNodeEntity entity : nnes) {			
			if (companyFilter!=null)
				if (!entity.getCompany().equalsIgnoreCase(companyFilter))
					continue;
			
			if (areaFilter!=null) {
				String filter = "X";
				if (areaFilter.equals("APP")) 
					filter = "A";
				else
				if (areaFilter.equals("INT"))
					filter = "I";				
				else
				if (areaFilter.equals("SIM"))
					filter = "S";
				
				boolean found=false;
				for (AreaEntity a: entity.getAreas())
					if (a.getName().equalsIgnoreCase(filter)) { 
						found=true;
						break;
					}
				if (!found)
					continue;
			}
			
			Set<HostEntity> hosts = entity.getHosts();
			HostEntity host = hosts.iterator().next();
			if (host != null)
				map.put(entity.getName(), host.getName());
		}
		return map;
	}
	
	/*
	 * public int getVersion() { return version; }
	 */

	// ---------------------------------------------------------------

	public void createResource(com.orsyp.migration.datamodel.du.Resource res) throws Exception {
		Context ctx = getContext();
		ResourceId ident = ResourceId.create(res.getDuName());
		Resource obj = new Resource(ctx, ident);
		if (res.label!=null)
			obj.setLabel(cutString(res.label,40));
		obj.setImpl(new OwlsResourceImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());

		if (res.scanFrequency != null) {
			int freq = Integer.parseInt(res.scanFrequency);
			if (freq<30)
				freq=30;
			obj.setFrequency(freq);			
		}
		else
			obj.setFrequency(60);

		switch (res.resourceType) {
		case FILE:
			obj.setNature(Resource.NATURE_RES_FILE);
			
			String dir = "";
			String fName = res.file;				
			
			File f = new File(res.file);
			if (f.getParent() != "") {
				dir = f.getParent();
				fName = f.getName();				
			}	
			
			SpecificFile spec = new SpecificFile();
			spec.setSyntaxRules(OwlsSyntaxRules.getInstance());
			spec.setName(fName);			
			if ((dir != null) && (dir != "")) {
				dir = res.file.replace(f.getName(),"");
				if (dir.endsWith("/") || dir.endsWith("\\"))
					dir = dir.substring(0,dir.length()-1);
				spec.setPath(dir);
			}
			obj.setSpecificData(spec);
			break;
		case SYSTEM:
			obj.setNature(Resource.NATURE_RES_SYS);
			SpecificSystem specs = new SpecificSystem();
			specs.setName("STORAGE");
			specs.setFileSystem(res.file);
			obj.setSpecificData(specs);
			break;
		case LOGICAL:
			obj.setNature(Resource.NATURE_RES_LOGICAL);
			SpecificLogical specl = new SpecificLogical();
			specl.setExclusive(false);
			if (res.quota1!=null)
				specl.setQuota1(Integer.parseInt(res.quota1));
			else
				specl.setQuota1(1);
			if (res.quota2!=null)
				specl.setQuota2(Integer.parseInt(res.quota2));
			else
				specl.setQuota2(0);
			obj.setSpecificData(specl);
			break;
		}

		obj.create();
	}

	public void deleteResource(String resName) throws Exception {
		Context ctx = getContext();
		Resource obj = new Resource(ctx, ResourceId.create(resName));
		obj.setImpl(new OwlsResourceImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}

	public void createMU(DuMu mu) throws Exception {
		Context ctx = getContext();
		Mu obj = new Mu(ctx, mu.getDuName());
		obj.setImpl(new OwlsMuImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		if (mu.label != null)
			obj.setLabel(cutString(mu.label,60));
		if (mu.node != null)
			obj.setNodeName(mu.node);
		else
			obj.setNodeName(referenceNode);
		obj.setDevelopmentAuth(true);
		obj.setProductionAuth(true);
		obj.setTimezone(new Timezone(Timezone.Sign.PLUS, 0, 0));
		obj.create();
	}


	public void createRule(DuRule r) throws Exception {
		Rule obj = new Rule(getContext(), RuleId.create(r.getDuName()));

		if (r.label != null)
			obj.setLabel(cutString(r.label,40)); //for some unknown reason uvms does not accept full length labels (60 chars)
		
		obj.setPeriodNumber(r.period.amount);
		
		obj.setPeriodType(PeriodTypeEnum.DAY);
		switch (r.period.period) {		
			case CLOSING:	obj.setPeriodType(PeriodTypeEnum.CLOSED_DAY); break;
			case WORKING:	obj.setPeriodType(PeriodTypeEnum.OPEN_DAY); break;
			case HOLIDAY:	obj.setPeriodType(PeriodTypeEnum.HOLIDAY); break;
			case DAY:		obj.setPeriodType(PeriodTypeEnum.DAY); break;
			case WEEK: 		obj.setPeriodType(PeriodTypeEnum.WEEK); break;
			case MONTH: 	obj.setPeriodType(PeriodTypeEnum.MONTH); break;
			case QUARTER: 	obj.setPeriodType(PeriodTypeEnum.QUARTER); break;
			case YEAR:	 	obj.setPeriodType(PeriodTypeEnum.YEAR); break;
			case MONDAY:	obj.setPeriodType(PeriodTypeEnum.MONDAY); break;
			case TUESDAY:	obj.setPeriodType(PeriodTypeEnum.TUESDAY); break;
			case WEDNESDAY:	obj.setPeriodType(PeriodTypeEnum.WEDNESDAY); break;
			case THURSDAY:  obj.setPeriodType(PeriodTypeEnum.THURSDAY); break;
			case FRIDAY:	obj.setPeriodType(PeriodTypeEnum.FRIDAY); break;
			case SATURDAY:	obj.setPeriodType(PeriodTypeEnum.SATURDAY); break;
			case SUNDAY:	obj.setPeriodType(PeriodTypeEnum.SUNDAY); break;
			case FISCALMONTH:	obj.setPeriodType(PeriodTypeEnum.FISCAL_MONTH); break;
			case FISCALQUARTER:	obj.setPeriodType(PeriodTypeEnum.FISCAL_QUARTER); break;
			case FISCALSEMESTER:obj.setPeriodType(PeriodTypeEnum.FISCAL_SEMESTER); break;
			case FISCALYEAR:	obj.setPeriodType(PeriodTypeEnum.FISCAL_YEAR); break;
		}		

		UniPattern pattern = new UniPattern();

		PositionsInPeriod positionsInPeriod = new PositionsInPeriod();
		positionsInPeriod.setPositionsPattern(""+r.period.positionNumber);
		
		positionsInPeriod.setForward(r.period.positionDirection==PositionType.FORWARD);
		
		positionsInPeriod.setType(SubPeriodType.DAY);
		switch(r.period.positionDayType) {
			case WORKING:	positionsInPeriod.setType(SubPeriodType.OPEN_DAY); break;
			case CLOSING:	positionsInPeriod.setType(SubPeriodType.CLOSED_DAY); break;
			case HOLIDAY:	positionsInPeriod.setType(SubPeriodType.HOLIDAY); break;
			case MONDAY:	positionsInPeriod.setType(SubPeriodType.MONDAY); break;
			case TUESDAY:	positionsInPeriod.setType(SubPeriodType.TUESDAY); break;
			case WEDNESDAY:	positionsInPeriod.setType(SubPeriodType.WEDNESDAY); break;
			case THURSDAY: 	positionsInPeriod.setType(SubPeriodType.THURSDAY); break;
			case FRIDAY:	positionsInPeriod.setType(SubPeriodType.FRIDAY); break;
			case SATURDAY:	positionsInPeriod.setType(SubPeriodType.SATURDAY); break;
			case SUNDAY:	positionsInPeriod.setType(SubPeriodType.SUNDAY); break;
		}

		pattern.setPositionsInPeriod(positionsInPeriod);
		
		pattern.setRunOver(RunOverEnum.NO);

		Hashtable<KDayType, KDayAuthorization> dayAuthorizations = new Hashtable<KDayType, KDayAuthorization>();
		
		//all unauthorized by default
		KDayAuthorization openDay = new KDayAuthorization(KDayAuthorizationType.EXCLUDED);
		KDayAuthorization closeDay = new KDayAuthorization(KDayAuthorizationType.EXCLUDED);
		KDayAuthorization holiday = new KDayAuthorization(KDayAuthorizationType.EXCLUDED);
		//check for authorized days
		for (int i = 0; i < r.daysofweek.size(); i++) {
			//if a day is authorized, then its category is too
			DayOfWeek dw = r.daysofweek.get(i);				
			if (dw.holiday)
				holiday = new KDayAuthorization(KDayAuthorizationType.AUTHORIZED);
			if (dw.closing)
				closeDay = new KDayAuthorization(KDayAuthorizationType.AUTHORIZED);
			if (dw.workday)
				openDay = new KDayAuthorization(KDayAuthorizationType.AUTHORIZED);
		}
		//set day type authorizations
		dayAuthorizations.put(KDayType.OPEN, openDay);
		dayAuthorizations.put(KDayType.CLOSED, closeDay);
		dayAuthorizations.put(KDayType.HOLIDAY, holiday);		
		pattern.setDayAuthorizations(dayAuthorizations);

		Offset offset = new Offset();// "No shift"
		switch (r.offset) {
			case NEXT:		offset = Offset.NEXT; break;
			case PREVIOUS:	offset = Offset.PREVIOUS; break;
		}
		pattern.setOffset(offset);

		obj.setPattern(pattern);
		
		MonthAuthorization monthAuthorization = new MonthAuthorization();
		monthAuthorization.setDirection(Direction.FROM_BEGINNING);
		for (int i = 0; i < 31; i++)
			monthAuthorization.setAuthorization(i, r.days.contains(i+1));

		obj.setMonthAuthorization(monthAuthorization);

		YearAuthorization yearAuthorization = new YearAuthorization();
		for (int i = 0; i < 12; i++)
			yearAuthorization.setAuthorization(i, r.months.contains(i+1));
		obj.setYearAuthorization(yearAuthorization);
		
		WeekAuthorization weekAuthorization = new WeekAuthorization();
		for (int i = 0; i < 7; i++) {
			weekAuthorization.setBlankDay(i, false);
			weekAuthorization.setClosedDay(i, false);
			weekAuthorization.setWorkedDay(i, false);
		}		
		for (int i = 0; i < r.daysofweek.size(); i++) {
			DayOfWeek dw = r.daysofweek.get(i);
			weekAuthorization.setBlankDay(dw.dayofweek, dw.holiday);
			weekAuthorization.setClosedDay(dw.dayofweek, dw.closing);
			weekAuthorization.setWorkedDay(dw.dayofweek, dw.workday);
		}

		obj.setWeekAuthorization(weekAuthorization);		

		obj.setImpl(new OwlsRuleImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.create();
	}

	public void deleteRule(String ruleName) throws Exception {
		Context ctx = getContext();
		Rule obj = new Rule(ctx, RuleId.create(ruleName));
		obj.setImpl(new OwlsRuleImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	
	public Rule getRule(String name) throws Exception{  
		Rule obj = new Rule(getContext(), RuleId.create(name));
	    obj.setImpl(new OwlsRuleImpl());
	    obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
	    obj.extract();
	    return obj;
	}
	
	public RuleList getRuleList() throws Exception {  
        RuleFilter filter = new RuleFilter("*","*");
        RuleList list = new RuleList(getContext(), filter);
        list.setImpl(new OwlsRuleListImpl());
        list.extract();
        return list;
	}
	
	public List<String> getRuleNames(){
		ArrayList<String> list = new ArrayList<String>();
		try {
	        RuleList r = getRuleList();        
	        for (int i = 0; i<r.getCount(); i++) {
	        	RuleItem ri = r.get(i);
	        	list.add(ri.getName());
			}
		} catch (Exception e) {}
        return list;
	}
	
	@SuppressWarnings("deprecation")
	public List<DuRule> getRules(){
		ArrayList<DuRule> list = new ArrayList<DuRule>();
		try {
	        RuleList r = getRuleList();        
	        for (int i = 0; i<r.getCount(); i++) {
	        	RuleItem ri = r.get(i);
	        	Rule rul = getRule(ri.getName());
	        	DuRule dur = new DuRule(rul.getName());
	        	dur.duDefaultRule = true;
	        	dur.label = rul.getLabel();
	        	dur.mappedName = dur.name;
	        	int idx=1;
	        	for (boolean b : rul.getYearAuthorization().getAuthorizations()) {
	        		if (b)
	        			dur.months.add(idx);
	        		idx++;
	        	}
	        	idx=1;
	        	for (boolean b : rul.getMonthAuthorization().getAuthorizations()) {
	        		if (b)
	        			dur.days.add(idx);
	        		idx++;
	        	}
	        	
	        	for (idx = 0; idx<7; idx++) 
	        		dur.daysofweek.add( new DayOfWeek(idx, 
	        									rul.getWeekAuthorization().getWorkedDay(idx), 
						        				rul.getWeekAuthorization().getClosedDay(idx), 
						        				rul.getWeekAuthorization().getBlankDay(idx)));
	        	
	        	dur.period = new Period();
	        	
	        	if (rul.getUniversePattern().getPositionsInPeriod().isForward()) 
		        	dur.period.positionDirection = PositionType.FORWARD;
	        	else
		        	dur.period.positionDirection = PositionType.BACKWARD;
	        		        	
	        	dur.period.positionNumber = Integer.parseInt(rul.getUniversePattern().getPositionsInPeriod().getPositionsPattern());
	        	dur.period.amount = rul.getPeriodNumber();
	        	switch (rul.getUniversePattern().getOffsetValue()) {
		        	case NEXT: 		dur.offset = DuRule.Offset.NEXT; break;
		        	case NONE:   	dur.offset = DuRule.Offset.NONE; break;
		        	case PREVIOUS:  dur.offset = DuRule.Offset.PREVIOUS; break;
	        	}
	        	
	        	switch (rul.getPeriodType()) {
		        	case CLOSED_DAY:	dur.period.period = PeriodType.CLOSING; break;
					case OPEN_DAY:		dur.period.period = PeriodType.WORKING; break;
					case HOLIDAY:		dur.period.period = PeriodType.HOLIDAY; break;
					case DAY:			dur.period.period = PeriodType.DAY; break;
					case WEEK: 			dur.period.period = PeriodType.WEEK; break;
					case MONTH: 		dur.period.period = PeriodType.MONTH; break;
					case QUARTER: 		dur.period.period = PeriodType.QUARTER; break;
					case YEAR:	 		dur.period.period = PeriodType.YEAR; break;
					case MONDAY:		dur.period.period = PeriodType.MONDAY; break;
					case TUESDAY:		dur.period.period = PeriodType.TUESDAY; break;
					case WEDNESDAY:		dur.period.period = PeriodType.WEDNESDAY; break;
					case THURSDAY:  	dur.period.period = PeriodType.THURSDAY; break;
					case FRIDAY:		dur.period.period = PeriodType.FRIDAY; break;
					case SATURDAY:		dur.period.period = PeriodType.SATURDAY; break;
					case SUNDAY:		dur.period.period = PeriodType.SUNDAY; break;
					case FISCAL_MONTH:	dur.period.period = PeriodType.FISCALMONTH; break;
					case FISCAL_QUARTER:dur.period.period = PeriodType.FISCALQUARTER; break;
					case FISCAL_SEMESTER:dur.period.period = PeriodType.FISCALSEMESTER; break;
					case FISCAL_YEAR:	dur.period.period = PeriodType.FISCALYEAR; break;
	        	}
	        	
	    		switch(rul.getUniversePattern().getPositionsInPeriod().getType()) {
	    			case DAY:		dur.period.positionDayType = Period.DayType.CALENDAR; break;
	    			case OPEN_DAY:	dur.period.positionDayType = Period.DayType.WORKING; break;
	    			case CLOSED_DAY:dur.period.positionDayType = Period.DayType.CLOSING; break;
	    			case HOLIDAY:	dur.period.positionDayType = Period.DayType.HOLIDAY; break;
	    			case MONDAY:	dur.period.positionDayType = Period.DayType.MONDAY; break;
	    			case TUESDAY:	dur.period.positionDayType = Period.DayType.TUESDAY; break;
	    			case WEDNESDAY:	dur.period.positionDayType = Period.DayType.WEDNESDAY; break;
	    			case THURSDAY: 	dur.period.positionDayType = Period.DayType.THURSDAY; break;
	    			case FRIDAY:	dur.period.positionDayType = Period.DayType.FRIDAY; break;
	    			case SATURDAY:	dur.period.positionDayType = Period.DayType.SATURDAY; break;
	    			case SUNDAY:	dur.period.positionDayType = Period.DayType.SUNDAY; break;
	    		}
	        	
	        	list.add(dur);
			}
		} catch (Exception e) {e.printStackTrace();}
        return list;
	}
	
	
	public List<Object[]> getUsers() throws Exception {
		ArrayList<Object[]> users = new ArrayList<Object[]>();
		try {
			UserFilter filter = new UserFilter("*","*");
	        UserList list = new UserList(getContext(), filter);
	        list.setImpl(new OwlsUserListImpl());
	        list.extract();
	        
	        for (int i = 0; i<list.getCount(); i++) {
	        	UserItem item = list.get(i);
	        	users.add(new Object[] {item.getName(),item.getUserSystem().getType().toString()});
			}
		} catch (Exception e) {}
        return users;
	}
	
	public List<Object[]> getMus() throws Exception {
		ArrayList<Object[]> mus = new ArrayList<Object[]>();
		try {
			MuFilter filter = new MuFilter("*","*");
	        MuList list = new MuList(getContext(), filter);
	        list.setImpl(new OwlsMuListImpl());
	        list.extract();
	        
	        for (int i = 0; i<list.getCount(); i++) {
	        	MuItem item = list.get(i);
	        	mus.add(new Object[] {item.getName(),item.getNodeName()});
			}
		} catch (Exception e) {}
        return mus;
	}
	
	public void createUser(DuUser user) throws Exception {
		User obj = new User(getContext(), user.getDuNameOriginalCase());
		obj.setAuthorCode("");
		obj.setProfile("profadm");
		obj.setUserType(UserType.Both);
		obj.setLabel(cutString(user.label,60));

		if (user.os==OSType.WINDOWS) {
			UserSystem userSystem = new UserSystem();
			userSystem.setUser(sysUser);
			userSystem.setPsw(sysPassword);
			userSystem.setType(sysType );
			obj.setUserSystem(userSystem);
		}
		else {
			UserSystem userSystem = new UserSystem();
			userSystem.setUser(user.getDuNameOriginalCase());
			userSystem.setPsw("");
			userSystem.setType(UserSystem.Type.OTHER);
			obj.setUserSystem(userSystem);
		}

		obj.setImpl(new OwlsUserImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.create();
	}

	public void deleteUser(String userName) throws Exception {
		Context ctx = getContext();
		User obj = new User(ctx, UserId.create(userName));
		obj.setImpl(new OwlsUserImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}

	public void createSession(com.orsyp.migration.datamodel.du.Session s) throws Exception {
		SessionId sessionId = new SessionId(s.getDuName(), defaultVersion);
		Session sess = new Session(getContext(), sessionId);
		if (s.label != null)
			sess.setLabel(cutString(s.label,40));

		SessionAtom root = new SessionAtom(new SessionData(s.header.getDuName()));
		ArrayList<String> usedUprs = new ArrayList<String>();
		addSessionChildren(s, root, s.header, usedUprs);
		sess.setTree(new SessionTree(root));

		sess.setImpl(new OwlsSessionImpl());
		sess.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		sess.create();
	}

	private void addSessionChildren(com.orsyp.migration.datamodel.du.Session s,
									SessionAtom parentAtom,
									com.orsyp.migration.datamodel.du.UProc parentUproc,
									ArrayList<String> usedUprs)
									throws InvalidMuException {
		SessionAtom lastAtomOk = null;
		SessionAtom lastAtomKo = null;

		for (SessionTreeElement el : s.descriptions) {
			if (!usedUprs.contains(el.uproc.getDuName()))
				if (el.father == parentUproc) {
					SessionAtom atom = null;
					if (el.mu != null)
						atom = new SessionAtom(new SessionData(el.uproc.getDuName(), new ExecutionContext(el.mu.getDuName())));
					else
						atom = new SessionAtom(new SessionData(el.uproc.getDuName()));
	
					if (el.success) {
						if (lastAtomOk == null)
							parentAtom.setChildOk(atom);
						else
							lastAtomOk.setNextSibling(atom);
						lastAtomOk = atom;
					} else {
						if (lastAtomKo == null)
							parentAtom.setChildKo(atom);
						else
							lastAtomKo.setNextSibling(atom);
						lastAtomKo = atom;
					}
					
					usedUprs.add(el.uproc.getDuName());
	
					addSessionChildren(s, atom, el.uproc, usedUprs);
				}
		}
	}
	
	public void createApp(String appName, String label) throws Exception {
        Application obj = new Application(getContext(), appName);
        obj.setDomain("I");
        obj.setLabel(label);
        obj.setImpl(new OwlsApplicationImpl());
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
        obj.create();
	}
	
	public void updateUproc(com.orsyp.migration.datamodel.du.UProc u, TemplateTable templates, boolean createCommandVar) throws Exception {
		Uproc obj = getUProc(u.getDuName());
		setUproc(obj, u, templates, false, createCommandVar);
	}

	public void createUproc(com.orsyp.migration.datamodel.du.UProc u, TemplateTable templates, boolean createCommandVar) throws Exception { 
		UprocId uprocId = new UprocId(u.getDuName(), defaultVersion);
		Uproc obj = new Uproc(getContext(), uprocId);
		setUproc(obj, u, templates, true, createCommandVar);
	}

	private void setUproc(Uproc obj, com.orsyp.migration.datamodel.du.UProc u, TemplateTable templates, boolean create, boolean createCommandVar) throws Exception {		
		if (u.label != null)
			obj.setLabel(cutString(u.label,60));
		if (u.info != null)
			obj.setDefaultInformation(cutString(u.info,50));
		obj.setDefaultSeverity(0);
		if (u.severity != null)
			try {
				obj.setDefaultSeverity(Integer.parseInt(u.severity));
			} catch (Exception e) {}
		obj.setApplication(u.applicationId);
		obj.setDomain(getDomain(u.applicationId));
		obj.setType(u.commandType.toString());
		obj.setFunctionalPeriod(getFunctionaPeriod(u.fPeriod));

		switch (u.commandType) {
			case CMD:	if (u.command != null) {
							CmdData cmdData = new CmdData();								
							cmdData.setCommandLine(u.command);
							if (u.workdir!=null)
								cmdData.setExecutionDirectory(u.workdir);
							obj.setSpecificData(cmdData);
						}
						break;
							
			case CL_EXT: ExternalScript extScript = new ExternalScript(obj, u.template);
        				obj.setExternalScript(extScript);
        				obj.setExternal(true); 
        				break;
            				
			case CL_INT: break; //script created after uproc creation					
		}

		obj.setImpl(new OwlsUprocImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		if (create)
			obj.create();
		else
			obj.update();
		
		//script file is created after object creation
		if (u.commandType == UProcType.CL_INT) 
			createInternalScript(u, templates, obj);

		// mem info
		Memorization memo = new Memorization();
		memo.setNumber(Integer.parseInt(u.eventNumber));
		// o,n,a = one,none,all
		if (memo.getNumber()==0)
			memo.setType(Type.NONE);
		else
			memo.setType(Type.ONE);			
				
		obj.setMemorization(memo);

		// add uproc variables
		Vector<Variable> varVector = new Vector<Variable>();
		if (u.variables != null) 
			for (DuVariable var : u.variables) {
				// additional options for date and number variables
				if (var.type.toUpperCase().equals("D")) {
					if (var.format != null) {
						VariableDate varDate = new VariableDate();
						varDate.setName(var.name);
						varDate.setValue(var.value);
						varDate.setOrigin(Variable.ORIGIN_UPROC);
						varDate.setFormat(var.format);
						varVector.add(varDate);
					}

				} else if (var.type.toUpperCase().equals("Q")) {
					VariableNumeric varNum = new VariableNumeric();
					varNum.setName(var.name);
					varNum.setValue(var.value);
					varNum.setOrigin(Variable.ORIGIN_UPROC);
					if (var.min != null)
						varNum.setMin(Integer.parseInt(var.min));
					if (var.max != null)
						varNum.setMax(Integer.parseInt(var.max));
					varVector.add(varNum);
				} else {
					VariableText varText = new VariableText();
					varText.setName(var.name);
					varText.setValue(var.value);
					varText.setLength(255);
					varText.setOrigin(Variable.ORIGIN_UPROC);
					varVector.add(varText);
				}
			}

		if ((u.commandType == UProcType.CL_EXT) || (u.commandType == UProcType.CL_INT)) {
			if (u.command != null) 
				if (!uprocHasVariable(u,"COMMAND") && createCommandVar)	{
					VariableText varText = new VariableText();
					varText.setName("COMMAND");
					varText.setValue(u.command);
					varText.setLength(255);
					varText.setOrigin(Variable.ORIGIN_UPROC);
					varVector.add(varText);
					if (u.workdir!=null) { 
						varText = new VariableText();
						varText.setName("WORKDIR");
						varText.setValue(u.workdir);
						varText.setLength(255);
						varText.setOrigin(Variable.ORIGIN_UPROC);
						varVector.add(varText);
					}
				}
		}

		if (varVector.size() > 0)
			obj.setVariables(varVector);

		// uproc exit status management
		if (u.statusManagement != null) {
			StatusManagement statusManagement = new StatusManagement();
			statusManagement.setEnabled(true);
			statusManagement.setType(StatusManagement.Type.RETURN_CODE);
			statusManagement.setStatus(StatusManagement.Status.COMPLETED);
			if (u.statusManagement.status == com.orsyp.migration.datamodel.du.StatusManagement.ManagedStatus.ABORTED)
				statusManagement.setStatus(StatusManagement.Status.ABORTED);
			if (u.statusManagement.extendend!=null) {
				ReturnCodePattern pat = new ReturnCodePattern(u.statusManagement.extendend);
				statusManagement.setPattern(pat);
			}
			else
			if (u.statusManagement.returnCode!=null) {
				statusManagement.setOperator(StatusManagement.Operator.EQUAL);
				statusManagement.setReturnCode(Integer.parseInt(u.statusManagement.returnCode));
	
				if (u.statusManagement.operator != null) {
					if (u.statusManagement.operator.equals("NE"))
						statusManagement.setOperator(StatusManagement.Operator.DIFFERENT);
					else if (u.statusManagement.operator.equals("GT"))
						statusManagement.setOperator(StatusManagement.Operator.GREATER);
					else if (u.statusManagement.operator.equals("LO"))
						statusManagement.setOperator(StatusManagement.Operator.LESS);
					else if (u.statusManagement.operator.equals("EQ"))
						statusManagement.setOperator(StatusManagement.Operator.EQUAL);
				}
				obj.setStatusManagement(statusManagement);
			}
		}

		// retries
		if (u.maxRetries != -1) {
			AutoRetry autoRetry = new AutoRetry();
			autoRetry.setDelay(u.retryWaitMinutes);
			autoRetry.setCount(u.maxRetries);
			autoRetry.setEnabled(true);
			obj.setAutoRetry(autoRetry);
		}

		// duration controls
		JobDuration jd = getDurationControl(com.orsyp.api.job.JobDuration.Type.MAX_DURATION, u.maxDuration);
		if (jd != null)
			obj.setMaxDuration(jd);
		jd = getDurationControl(com.orsyp.api.job.JobDuration.Type.MIN_DURATION, u.minDuration);
		if (jd != null)
			obj.setMinDuration(jd);
		// wait duration
		jd = getDurationControl(com.orsyp.api.job.JobDuration.Type.MAXBR,u.maxWaitDuration);
		if (jd != null)
			obj.setMaxBrDuration(jd);
		
		//mail notifications
		if (u.mailTo!=null && u.mailTo.size()>0) {
			EmailNotification em = new EmailNotification();
			em.setEnabled(true);
			em.setCompleted(true);
			em.setAborted(false);
			em.setRefused(false);
			em.setOverrun(false);
			em.setJobLog(u.mailParams!=null && u.mailParams.contains("log"));
			Vector<String> addr = new Vector<String>();
			for (String recip : u.mailTo)				
				addr.add(recip);
			em.setAddresses(addr);
			obj.setEmailNotification(em);
		}

		obj.update();
	}


	
	private String getDomain(String applicationId) throws Exception {
		String domain = appDomains.get(applicationId);
		if (domain==null) {
			Application app = new Application(getContext(), applicationId); 
			app.setImpl(new OwlsApplicationImpl());
            app.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
            app.extract();
			domain = app.getDomain();
			appDomains.put(applicationId, domain);
		}			
		return domain;
	}

	public JobDuration getDurationControl(com.orsyp.api.job.JobDuration.Type type, DurationControl dur) {
		if (dur != null) {
			JobDuration jobDuration = new JobDuration();
			jobDuration.setType(type);
			if (dur.percent>0) {
				jobDuration.setDurationType(DurationType.STATS);
				jobDuration.setStatDuration(dur.percent);
			}
			else {
				jobDuration.setDurationType(DurationType.EXPLICIT);
				jobDuration.setExpliDuration(String.format("%03d%02d%02d", dur.hours,dur.minutes,dur.seconds));
			}

			String action = "F";
			if (dur.action != null)
				action = dur.action;

			if (action.equals("F"))
				jobDuration.setAction(KActionType.FORCE_ABORT);
			else {
				jobDuration.setAction(KActionType.CUSTOM_SCRIPT);				
				jobDuration.setPath(escapePath(dur.script));
			}
			
			return jobDuration;
		}
		return null;
	}

	private FunctionalPeriod getFunctionaPeriod(String fp) {
		if (fp.equalsIgnoreCase("N"))
			return FunctionalPeriod.None;
		if (fp.equalsIgnoreCase("D"))
			return FunctionalPeriod.Day;
		if (fp.equalsIgnoreCase("T"))
			return FunctionalPeriod.TenDays;
		if (fp.equalsIgnoreCase("W"))
			return FunctionalPeriod.Week;
		if (fp.equalsIgnoreCase("F"))
			return FunctionalPeriod.TwoWeeks;
		if (fp.equalsIgnoreCase("M"))
			return FunctionalPeriod.Month;
		if (fp.equalsIgnoreCase("2"))
			return FunctionalPeriod.TwoMonths;
		if (fp.equalsIgnoreCase("3"))
			return FunctionalPeriod.ThreeMonths;
		if (fp.equalsIgnoreCase("Q"))
			return FunctionalPeriod.FourMonths;
		if (fp.equalsIgnoreCase("S"))
			return FunctionalPeriod.SixMonths;
		if (fp.equalsIgnoreCase("Y"))
			return FunctionalPeriod.Year;
		return null;
	}

	public void createTerminationInstructions(com.orsyp.migration.datamodel.du.UProc u, GenericDataModel genModel, DUDataModel duModel) throws Exception {
		// completion instructions
		Vector<CompletionInstruction> comp = new Vector<CompletionInstruction>();

		for (TerminationInstruction t : u.terminationInstructions) {

			CompletionInstruction completionInstruction = new CompletionInstruction();
			completionInstruction.setStatus(CompletionInstruction.Status.COMPLETED);
			if (t.status == UProcStatus.ABORTED)
				completionInstruction.setStatus(CompletionInstruction.Status.ABORTED);

			completionInstruction.setFunctionalPeriod(FunctionalPeriod.Day);
			completionInstruction.setUproc(t.uproc.getDuName());
			
			completionInstruction.setProcessingDateControl(getDateControl(t.checks));
			completionInstruction.setSessionControl(getSessionControl(t.checks, duModel));
			if (t.checks.userCheck==CheckType.ANY)
				completionInstruction.setUserControl(UserControl.ANY);
			else
				completionInstruction.setUserControl(UserControl.SAME);
			completionInstruction.setMuControl(getMuControl(t.checks, genModel));
			

			comp.add(completionInstruction);
		}

		if (comp.size() > 0) {
			Uproc obj = getUProc(u.getDuName());
			obj.setCompletionInstructions(comp);
			obj.update();
		}

	}

	public Uproc getUProc(String name) throws Exception {
		UprocId uprocId = new UprocId(name, defaultVersion);
		Uproc u = new Uproc(getContext(), uprocId);
		u.setImpl(new OwlsUprocImpl());
		u.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		u.extract();
		return u;
	}
	
	public Session getSession(String name) throws Exception {
		SessionId sessionId = new SessionId(name, defaultVersion);
		Session ses = new Session(getContext(), sessionId);
		ses.setImpl(new OwlsSessionImpl());
		ses.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		ses.extract();
		return ses;
	}

	public void createDependencies(UProc u, GenericDataModel genModel, DUDataModel duModel) throws Exception {
		if (u.conditions.size() == 0)
			return;

		Vector<DependencyCondition> conDep = new Vector<DependencyCondition>();
		Vector<NonSimultaneityCondition> conNon = new Vector<NonSimultaneityCondition>();
		Vector<ResourceCondition> conRes = new Vector<ResourceCondition>();

		for (Condition c : u.conditions) {
			UniverseCondition cond = null;
			
			//user control
			UserControl userControl = UserControl.ANY;
			if (c.conditionChecks.userCheck != CheckType.ANY)
				userControl = UserControl.SAME;
			
			switch (c.type) {
				case DEPENDENCY:
					DependencyCondition depcond = new DependencyCondition();
					depcond.setFunctionalPeriod(getFunctionaPeriod(u.fPeriod)); 
					depcond.setUproc(duModel.uprocs.get(c.OriginId).getDuName());
					//default COMPLETED
					depcond.setStatus(Status.COMPLETED);
					switch (c.uprocStatus) {	 
						case ABORTED: depcond.setStatus(Status.ABORTED); break;
						case ABSENT:  depcond.setStatus(Status.ABSENT); break;
					}								
					depcond.setSessionControl(getSessionControl(c.conditionChecks,duModel));
					depcond.setUserControl(userControl);				
					depcond.setProcessingDateControl(getDateControl(c.conditionChecks));
					cond = depcond;
					break;
				case RESOURCE:
					ResourceCondition rescond = new ResourceCondition();
					com.orsyp.migration.datamodel.du.Resource duRes = duModel.resources.get(c.OriginId); 
					rescond.setResource(duRes.getDuName());
					
					switch (duRes.resourceType) {
						case FILE:		rescond.setType(ResourceCondition.Type.FILE); break;
						case LOGICAL:	rescond.setType(ResourceCondition.Type.LOGICAL); break;
						case SYSTEM:	rescond.setType(ResourceCondition.Type.SYS); break;
						case SCRIPT:	rescond.setType(ResourceCondition.Type.SCR); break;
						case SAP_EVENT:	rescond.setType(ResourceCondition.Type.SAP_EVENT); break;
						case SAP_JOB:	rescond.setType(ResourceCondition.Type.SAP_JOB); break;
					}
					
					if (c.attribute!=null)
						rescond.setAttribute(c.attribute.toString()); 
					else
						rescond.setAttribute("EXIST");
					if ((c.amountOperator != null) && (c.amountValue != null)) {
						// # | = | < | > | <= | >=
						if (c.amountOperator.equals("="))
							rescond.setOperator(Operator.EQUAL);
						else if (c.amountOperator.equals("#"))
							rescond.setOperator(Operator.DIFFERENT);
						else if (c.amountOperator.equals("<"))
							rescond.setOperator(Operator.LESS);
						else if (c.amountOperator.equals(">"))
							rescond.setOperator(Operator.GREATER);
						else if (c.amountOperator.equals("<="))
							rescond.setOperator(Operator.LESS_OR_EQUAL);
						else if (c.amountOperator.equals(">="))
							rescond.setOperator(Operator.GREATER_OR_EQUAL);
						rescond.setAttributeValue(c.amountValue);
					}
					else {
						rescond.setOperator(Operator.EQUAL);
						if (c.amountValue != null)
							rescond.setNum(Integer.parseInt(c.amountValue));
					}
					
					if (c.quota1>0)
						rescond.setQuota1(c.quota1);
					if (c.quota2>0)
						rescond.setQuota2(c.quota2);
					
					rescond.setChecked(true);
					cond = rescond;
					break;
				case NON_SIMULTANEITY:
					NonSimultaneityCondition ncond = new NonSimultaneityCondition(); 
					ncond.setUproc(duModel.uprocs.get(c.OriginId).getDuName());
					ncond.setSessionControl(getSessionControl(c.conditionChecks,duModel));
					ncond.setUserControl(userControl);
					cond = ncond;
					break;
			}
						
			//Mu control		
			cond.setMuControl(getMuControl(c.conditionChecks, genModel));

			// common
			cond.setExpected(!c.exclude);
			cond.setFatal(false);
			cond.setNum(Integer.parseInt(c.getConditionNumber().replace("=C","")));			

			switch (c.type) {
				case DEPENDENCY:
					conDep.add((DependencyCondition) cond);
					break;
				case RESOURCE:
					conRes.add((ResourceCondition) cond);
					break;
				case NON_SIMULTANEITY:
					conNon.add((NonSimultaneityCondition) cond);
					break;
			}
		}

		if ((conDep.size() > 0) || (conRes.size() > 0) || (conNon.size() > 0)) {
			Uproc upr = getUProc(u.getDuName());
			if (conDep.size() > 0)
				upr.setDependencyConditions(conDep);
			if (conRes.size() > 0)
				upr.setResourceConditions(conRes);
			if (conNon.size() > 0)
				upr.setNonSimultaneityConditions(conNon);
			LaunchFormula f = new LaunchFormula();
			f.appendText(u.formula);
			upr.setFormula(f);
			upr.update();
		}
	}

	public void createTask(com.orsyp.migration.datamodel.du.Task t, DUDataModel model, boolean template) throws Exception {
		//scheduled tasks are created as template tasks
		Task obj = new Task(getContext(), TaskId.createWithName(t.getDuName(), defaultVersion, t.mu.getDuName(), template));
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
        obj.setImpl(new OwlsTaskImpl());
        if (t.label != null)
			obj.setLabel(cutString(t.label,60));
                
        /*
        obj.setAdvanceDays(0);
        obj.setAdvanceHours(0);
        obj.setAdvanceMinutes(0);
        obj.setAutoRestart(false);
        obj.setDayOffset(0);
        obj.setDeploy(true);
        obj.setEndExecutionForced(false);
        obj.setFlagAdvance(false);
        obj.setHistory(true);
        obj.setInteractiveFlag(false);
        obj.setParallelLaunch(true);
        obj.setPriority("100");
        obj.setSimulated(false);
        obj.setUpdate(true);
        */
        
        //set session and uproc
        if (t.sessionId!=null) {
        	com.orsyp.migration.datamodel.du.Session s = model.sessions.get(t.sessionId);
	        obj.getIdentifier().setSessionName(s.getDuName());
	        obj.getIdentifier().setSessionVersion(defaultVersion);
        }
        else
        if (t.type != com.orsyp.migration.datamodel.du.Task.Type.SCHEDULED && 
        	t.type != com.orsyp.migration.datamodel.du.Task.Type.PROVOKED) 
        	throw new Exception("Task " + t.getDuName() + " has no session. Task not created");
        
        UProc u = model.uprocs.get(t.uprocId);
        obj.getIdentifier().setUprocName(u.getDuName());
        obj.getIdentifier().setUprocVersion(defaultVersion);
        //user
        obj.setUserName(t.user.getDuNameOriginalCase());        
        //mu
        obj.setMuName(t.mu.getDuName());
                
        obj.setFunctionalPeriod(getFunctionaPeriod("D"));
        
        obj.setTypeDayOffset(com.orsyp.api.task.DayType.CALENDAR);
    	obj.setUnitOffset(0);
        if (t.pDatePreceedingDay) 
        	obj.setDayOffset(-1);
        else
        	if (t.dayOffset!=0)
        		obj.setDayOffset(t.dayOffset);
        	else
        		obj.setDayOffset(0);
        
        if (t.queue!=null && t.queue.length()>0)
        	obj.setQueue(t.queue);
        else
        	obj.setQueue("SYS_BATCH");
        obj.setPriority("100");
        obj.setPrinter("PRN");
                
        //task type
        switch (t.type) {
	        case SCHEDULED: obj.setTaskType(TaskType.Scheduled); break;
	        case OPTIONAL:	obj.setTaskType(TaskType.Optional); break;
	        case PROVOKED:	obj.setTaskType(TaskType.Provoked); break;
	        case CYCLICAL:	obj.setTaskType(TaskType.Cyclic); break;
        }        
        
        //task activation
        switch (t.activation) {
        	case ENABLED: 	obj.setActive(true); break;
			case DISABLED:	obj.setActive(false); break;
			case SIMULATED: obj.setSimulated(true); break;
		}
        
        //set parent task for provoked and optional tasks
        if ((t.type==com.orsyp.migration.datamodel.du.Task.Type.PROVOKED) 
             || (t.type==com.orsyp.migration.datamodel.du.Task.Type.OPTIONAL)) 
        	if (t.parentTask!=null) {
	        	obj.setParentTaskName(t.parentTask.getDuName());
	        	obj.setParentTaskMu(t.parentTask.mu.getDuName());
        	}
        	else
        		if (t.type==com.orsyp.migration.datamodel.du.Task.Type.OPTIONAL)
        			throw new Exception("Provoked/Optional task has no parent");
                
        if (t.type == com.orsyp.migration.datamodel.du.Task.Type.SCHEDULED)
        	if (!template)
        		obj.setActive(false);
        
        obj.setValidFrom(DateTools.toDate("20100101", "000000"));
                
        //add task scheduling
        if (t.type != com.orsyp.migration.datamodel.du.Task.Type.PROVOKED)
			createTaskScheduling(t, obj);
        
        //set ondemand provoked task schedule
        if (t.type==com.orsyp.migration.datamodel.du.Task.Type.PROVOKED)         	
        	if (t.pvkTime || t.setOnRequestTime){
        		List<TimeFrame> timeFrames = TimeFrame.splitCyclicTimeFrames(t.launchWindows);
        		for (TimeFrame ti : timeFrames) {
	        		TaskProvokedData dt=  new TaskProvokedData ();
	        		String start = String.format("%02d%02d00", ti.getStart().getHourOfDay(), ti.getStart().getMinuteOfHour());
	        		dt.setStartLaunchTime(start);
	            	dt.setNbHourElaps(String.format("%03d",ti.getDurationInMinutes() / 60));
	            	dt.setNbMinuteElaps(String.format("%02d",ti.getDurationInMinutes() % 60));
	            	dt.setStartExcludTime("000000");
	            	dt.setEndExcludTime("000000");
	            	obj.setSpecificData (dt);
        		}
        	}
        	else {
        		TaskProvokedData dt=  new TaskProvokedData ();
	        	dt.setStartLaunchTime("999999");
	        	dt.setNbHourElaps("999");
	        	dt.setNbMinuteElaps("59");
	        	dt.setStartExcludTime("000000");
	        	dt.setEndExcludTime("000000");
	        	obj.setSpecificData (dt);
        	}
               
		// launch variables
		if ((t.variables != null) && (t.variables.size()>0)) {
			ArrayList<Variable> vars = new ArrayList<Variable>();
			for (DuVariable v : t.variables) {
				VariableText var = new VariableText();
				var.setLength(255);
				var.setName(v.name);
				var.setValue(v.value);
				vars.add(var);
			}			
			obj.setVariables(vars);
		}

        obj.create();
	}

	public void createTaskScheduling(com.orsyp.migration.datamodel.du.Task t, Task obj) throws Exception {
        TaskPlanifiedData taskPlanifiedData = new TaskPlanifiedData();
        
        //set event creation for optional tasks: event is created even if the task is not running
        if (t.type==com.orsyp.migration.datamodel.du.Task.Type.OPTIONAL)
        	taskPlanifiedData.setGenerateEvent(true);
                
        // Rules        
        TaskImplicitData[] implicitDataArray = new TaskImplicitData[t.rules.size() + t.excludedRules.size()];
        
        int idx=0;
    	for (DuRule r : t.rules) {
    		Rule rule = getRule(r.getDuName());    		
			TaskImplicitData taskImplicitData = new TaskImplicitData(rule.getIdentifier());
			taskImplicitData.setFunctionalVersion(rule.getFunctionalVersion());
			taskImplicitData.setLabel(rule.getLabel());
			taskImplicitData.setMonthAuthorization(rule.getMonthAuthorization());
			taskImplicitData.setWeekAuthorization(rule.getWeekAuthorization());						
	        taskImplicitData.setPeriodType (rule.getPeriodType());
	        taskImplicitData.setPeriodNumber(rule.getPeriodNumber());
	        taskImplicitData.setPattern (rule.getPattern ());
	        taskImplicitData.setAuthorized(true); 
	        taskImplicitData.setReferenceDate("20100101");	        
	        taskImplicitData.setInternal(true); // so the rule is embedded	        
	        implicitDataArray[idx] = taskImplicitData;
	        idx++;
		}
    	
    	for (DuRule r : t.excludedRules) {
    		Rule rule = getRule(r.getDuName());
			TaskImplicitData taskImplicitData = new TaskImplicitData(rule.getIdentifier());
			taskImplicitData.setFunctionalVersion(rule.getFunctionalVersion());
			taskImplicitData.setLabel(rule.getLabel());
			taskImplicitData.setMonthAuthorization(rule.getMonthAuthorization());
			taskImplicitData.setWeekAuthorization(rule.getWeekAuthorization());				
	        taskImplicitData.setPeriodType (rule.getPeriodType());
	        taskImplicitData.setPeriodNumber(rule.getPeriodNumber());
	        taskImplicitData.setPattern (rule.getPattern ());
	        taskImplicitData.setAuthorized(false); 
	        taskImplicitData.setReferenceDate("20100101");
	        taskImplicitData.setInternal(true); // so the rule is embedded
	        
	        implicitDataArray[idx] = taskImplicitData;
	        idx++;
		}
    	
    	String startTime = "0000";
    	if (t.launchWindows.size()==1) {
    		startTime = String.format("%02d%02d", t.launchWindows.get(0).getStart().getHourOfDay(), t.launchWindows.get(0).getStart().getMinuteOfHour());
    	}
    	
    	for (String dt : t.dates) {
    		DateTime dtime = DateTime.parse(dt, DateTimeFormat.forPattern("mm/dd/yy"));
    		if (!dtime.isBeforeNow()) {    			
	    		String s = dtime.toString(DateTimeFormat.forPattern("yyyymmdd"));
	    		ExplicitLaunchDate ld = new ExplicitLaunchDate(s, startTime,"23","59", s);
	    		taskPlanifiedData.addExplicitDate(ld);
    		}
    	}
    	for (String dt : t.excludedDates) {
    		DateTime dtime = DateTime.parse(dt, DateTimeFormat.forPattern("mm/dd/yy"));
    		if (!dtime.isBeforeNow()) {
	    		String s = dtime.toString(DateTimeFormat.forPattern("yyyymmdd"));
	    		ExclusionDate ed = new ExclusionDate(s);
	    		taskPlanifiedData.addExclusionDate(ed);
    		}
    	}
    	
    	taskPlanifiedData.setImplicitData(implicitDataArray);    	
    	
    	//no launch windows for optional tasks
    	//TODO DEBUG removed for SSQ only
    	//if (t.type!=com.orsyp.migration.datamodel.du.Task.Type.OPTIONAL) {
	        idx=0;
	        if (t.launchWindows.size() > 0) {
	        	LaunchHourPattern[] launches = new LaunchHourPattern[t.launchWindows.size()];   
	        	for (TimeFrame ti : t.launchWindows) {
			        // Simple launches		                 
			        LaunchHourPattern lhp = new LaunchHourPattern();            
			        lhp.setStartTime(String.format("%02d%02d00", ti.getStart().getHourOfDay(), ti.getStart().getMinuteOfHour()));
			        lhp.setDurationHour(ti.getDurationInMinutes() / 60);
			        lhp.setDurationMinute(ti.getDurationInMinutes() % 60);
			        lhp.setDurationSecond(0);
			        
			        //in minutes
			        if (ti.getPeriod()>0) {
			        	lhp.setDurationHour(ti.getPeriod() / 60);
				        lhp.setDurationMinute(ti.getPeriod() % 60);
				        lhp.setDurationSecond(0);				        
			        	lhp.setFrequency(ti.getPeriod());
			        	lhp.setEndTime(String.format("%02d%02d00", ti.getEnd().getHourOfDay(), ti.getEnd().getMinuteOfHour()));
			        }
			        launches[idx] = lhp;
			        idx++;
	        	}
	        	taskPlanifiedData.setLaunchHourPatterns(launches);
	        }
    	//}

        //offset example
        /*
        Offset offset = new Offset();
        offset.setDayType(Offset.DayType.CALENDAR);
        offset.setValue(1);
        taskPlanifiedData.setOffset(offset);
        */

        obj.setSpecificData(taskPlanifiedData);
	}
	
	
	private MuControl getMuControl(ConditionChecks conditionChecks, GenericDataModel genModel) {
		MuControl muControl = new MuControl();
		muControl.setType(MuControl.Type.SAME_MU);
		if (conditionChecks.muCheck != CheckType.SAME)
			if (conditionChecks.muValue != null) {
				DuMu mu = genModel.commonObjects.mus.get(conditionChecks.muValue);
				muControl.setType(MuControl.Type.SPECIFIC_MU);
				muControl.setMu(mu.getDuName());				
			}
		return muControl;
	}
	
	private SessionControl getSessionControl(ConditionChecks conditionChecks, DUDataModel duModel){
		SessionControl sessionControl = new SessionControl();
		sessionControl.setType(SessionControl.Type.ANY_SESSION);
		if (conditionChecks.sessionCheck != CheckType.ANY)
			if (conditionChecks.sessionCheck == CheckType.SAMERUN)
				sessionControl.setType(SessionControl.Type.SAME_SESSION_AND_EXECUTION);
			else
			if (conditionChecks.sessionCheck == CheckType.SAME)
				sessionControl.setType(SessionControl.Type.SAME_SESSION);
			else 
			if (conditionChecks.sessionCheck == CheckType.VALUE)
				if (conditionChecks.sessionValue != null) {
					sessionControl.setType(SessionControl.Type.SPECIFIC_SESSION);
					sessionControl.setSession(duModel.sessions.get(conditionChecks.sessionValue).getDuName());
				}					
		return sessionControl;
	}
	
	private ProcessingDateControl getDateControl(ConditionChecks conditionChecks) {		
		//date control
		ProcessingDateControl processingDateControl = new ProcessingDateControl();
		
		CalendarUnit dayCalUnit = processingDateControl.new CalendarUnit();
		dayCalUnit.setDayType(DayType.CALENDAR);
        dayCalUnit.setFunctionalPeriod(FunctionalPeriod.Day);	        
		switch (conditionChecks.dayCheck) {
			case ANY:
				dayCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.ANY);				        
				break;
			case OFFSET:
				dayCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.PLUS);
				if (conditionChecks.dayValue.startsWith("-"))
					dayCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.MINUS);							
				dayCalUnit.setValue(Math.abs(Integer.parseInt(conditionChecks.dayValue)));
				break;
			case VALUE:
				if (conditionChecks.dayValue != null)
					dayCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.EQUAL);
					dayCalUnit.setValue(Integer.parseInt(conditionChecks.dayValue));
				break;
			case SAME:
				dayCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.SAME);
				break;
		}
		
		CalendarUnit monthCalUnit = processingDateControl.new CalendarUnit();
		monthCalUnit.setDayType(DayType.CALENDAR);
        monthCalUnit.setFunctionalPeriod(FunctionalPeriod.Day);
		switch (conditionChecks.monthCheck) {
			case ANY:
				monthCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.ANY);				        
				break;
			case OFFSET:
				monthCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.PLUS);
				if (conditionChecks.monthValue.startsWith("-"))
					monthCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.MINUS);							
				monthCalUnit.setValue(Math.abs(Integer.parseInt(conditionChecks.monthValue)));
				break;
			case VALUE:
				if (conditionChecks.monthValue != null)
					monthCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.EQUAL);
					monthCalUnit.setValue(Integer.parseInt(conditionChecks.monthValue));
				break;
			case SAME:
				monthCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.SAME);
				break;
		}
		
		CalendarUnit yearCalUnit = processingDateControl.new CalendarUnit();
		yearCalUnit.setDayType(DayType.CALENDAR);
        yearCalUnit.setFunctionalPeriod(FunctionalPeriod.Day);
        switch (conditionChecks.yearCheck) {
			case ANY:
				yearCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.ANY);				        
				break;
			case OFFSET:
				yearCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.PLUS);
				if (conditionChecks.yearValue.startsWith("-"))
					yearCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.MINUS);							
				yearCalUnit.setValue(Math.abs(Integer.parseInt(conditionChecks.yearValue)));
				break;
			case VALUE:
				if (conditionChecks.yearValue != null)
					yearCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.EQUAL);
					yearCalUnit.setValue(Integer.parseInt(conditionChecks.yearValue));
				break;
			case SAME:
				yearCalUnit.setOperator(com.orsyp.api.uproc.ProcessingDateControl.Operator.SAME);
				break;
		}
        
        processingDateControl.setCalendarUnitDay(dayCalUnit);
        processingDateControl.setCalendarUnitMonth(monthCalUnit);
        processingDateControl.setCalendarUnitYear(yearCalUnit);
                
        return processingDateControl;		
	}

	private void createInternalScript(UProc u, TemplateTable templates, Uproc obj) throws Exception {
		String scriptText = FileUtils.readFileToString(u.template);
		if (scriptText!=null) {
			scriptText = TemplateUtils.replaceKeywords(scriptText,templates.getKeywords().getKeywordMap(),u);		
	        InternalScript script = new InternalScript(obj);
	        script.setBinary(false);
	        script.setUproc(obj);
	        script.setLines(scriptText.split("\\\\n"));
	        script.save();
		}
		else
			throw new Exception("Error creating internal script. File: '" + u.template+ "'");
	}		
	
	
	public String escapePath(String script) {
		return script.replace("\\", "\\\\");
	}

	public String cutString(String input, int len) {
		if (input != null)
			return input.substring(0, Math.min(len - 1, input.length()));
		return " ";
	}
	
	
	public void testReadObject() throws Exception {
		uprocExists("XXXXXXXXXXXXXX");
	}
	
	public List<String> getDummyObjects() throws Exception {
		ArrayList<String> list = new ArrayList<String>();
		//read all objects from dummy application
		UprocFilter filter = new UprocFilter();        
        filter.setId("*");
        filter.setName("*");
        filter.setVersion(defaultVersion);
        filter.setLabel("*");
        filter.setApplication(BaseDuObject.DUMMY_UPROC_APP);
        filter.setDomain("*");
        
        UprocList l = new UprocList(getContext(), filter);
        l.setImpl(new OwlsUprocListImpl());
        l.extract(Operation.DISPLAY);
		
        for (int i=0; i<l.getCount(); i++) 
			list.add(l.get(i).getName());
		return list;
	}

	public boolean uprocExists(String uprName) throws Exception {
		//try {
			UprocFilter filter = new UprocFilter();        
	        filter.setId("*");
	        filter.setName(uprName);
	        filter.setVersion(defaultVersion);
	        filter.setLabel("*");
	        filter.setApplication("*");
	        filter.setDomain("*");
	        
	        UprocList l = new UprocList(getContext(), filter);
	        l.setImpl(new OwlsUprocListImpl());
	        l.extract(Operation.DISPLAY);
	        
	        return l.getCount()==1;
	        	
		//} catch (Exception e) {}
        
		//return false;
	}
	
	public void deleteMu(String muName) throws Exception {
		Mu obj = new Mu(getContext(), muName);
		obj.setImpl(new OwlsMuImpl());
		obj.delete();
	}

	public void deleteUProc(String name) throws Exception {
		Context ctx = getContext();
		Uproc obj = new Uproc(ctx, UprocId.create(name, name, defaultVersion));
		obj.setImpl(new OwlsUprocImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}

	public void deleteSession(String name) throws Exception {
		Context ctx = getContext();
		Session obj = new Session(ctx, SessionId.create(name, defaultVersion));
		obj.setImpl(new OwlsSessionImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}

	public void deleteTask(String name, String muName) throws Exception {
		Context ctx = getContext();
		Task obj = new Task(ctx, TaskId.createWithName(name, defaultVersion, muName, true));
		obj.setImpl(new OwlsTaskImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	
	public void createNodeVariable(String varName, String value) throws Exception {
		//a specific context is needed, node var creation works only on area X
		NodeVariable nv = new NodeVariable(makeContext(referenceNode,"X"),varName);
		nv.setImpl(new NodeVariableStdImpl());
		nv.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		nv.setVariableValue(value);
		nv.create();
	}
	
	public void createMUVariable(String mu, String varName, String value) throws Exception {
		TargetVariable targetVariable = new TargetVariable(getContext(), new TargetVariableId(mu, null, varName));
		targetVariable.setVariableValue(value);
		targetVariable.setImpl(new TargetVariableStdImpl());
		targetVariable.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());		
		targetVariable.create();
	}
	
	
	private boolean uprocHasVariable(UProc u, String varName) {
		for (DuVariable v : u.variables) 
			if (v.name.equals(varName))
				return true;
		return false;
	}

	@SuppressWarnings("deprecation")
	public List<ExecutionResult> getLaunches(Date start, Date end)  throws Exception {
		List<ExecutionResult> results = new ArrayList<ExecutionResult>();
				
		ExecutionFilter filter = new ExecutionFilter();
		filter.setBeginDate(new SimpleDateFormat("yyyyMMdd").format(start));
		filter.setEndDate(new SimpleDateFormat("yyyyMMdd").format(end));		
		filter.setBeginHour(new SimpleDateFormat("HHmmss").format(start));
		filter.setEndHour(new SimpleDateFormat("HHmmss").format(end));
		
		ExecutionList list = new ExecutionList(getContext(), filter);
		list.setImpl(new OwlsExecutionListImpl());
		list.extract(Operation.DISPLAY);
		
		for (int idx = 0; idx<list.getCount(); idx++) {
			ExecutionItem i = list.get(idx);
			ExecutionResult r = new ExecutionResult();
			r.uproc = i.getUprocName();
			r.session = i.getSessionName();
			r.mu = i.getMuName();
			r.status = i.getStatus().toString();
			r.procDate = i.getProcessingDate();
			r.beginDate = i.getBeginDate();
			r.endDate = i.getEndDate();				
			results.add(r);
		}
		
		System.out.println("" + results.size() + " executions from " + filter.getBeginDate() + "-" + filter.getBeginHour() + " to " + filter.getEndDate() + "-" + filter.getEndHour());
		for (ExecutionResult r : results)
			System.out.println("SESS:" +r.session + " - UPR:" + r.uproc + "  --  Status:" + r.status + " Start:" + r.beginDate.toGMTString() + " - End:" + r.endDate + " - Proc.date:" + r.procDate);
		
		return results;
	}

	public List<ExecutionResult> getSimulation(Date start, Date end, com.orsyp.migration.datamodel.du.Task t)  throws Exception {
		ArrayList<ExecutionResult> res = new ArrayList<ExecutionResult>();
		
		Task obj = new Task(getContext(), TaskId.createWithName(t.getDuName(), defaultVersion, t.mu.getDuName(), true));
        obj.setImpl(new OwlsTaskImpl());
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
        obj.extract();
        
        DetailedSimulation ds = obj.simulateFull(start, end);
        for( Date d : ds.getForecastDates() ) {
        	ExecutionResult ex = new ExecutionResult();
        	ex.session = t.sessionId;
        	ex.uproc = t.uprocId;
        	ex.mu = t.mu.getDuName();
        	ex.beginDate = d;
        	res.add(ex);
        }	
		return res;
	}
	
	
	public void deleteBV (String name) throws Exception {
		BusinessViewId id = new BusinessViewId(name.replace(".", "_")+".BV");
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		BusinessView bv = new BusinessView(id);
    	bv.setImpl(new OwlsBusinessViewImpl());    	    	
		bv.setIdentifier(id);
		bv.setContext(getContext());
		bv.delete();
	}
	
	public void createBV (com.orsyp.migration.datamodel.du.Task t, boolean template) throws Exception {
		BusinessViewId id = new BusinessViewId(t.getDuName().replace(".", "_")+".BV");
    	id.setSyntaxRules(OwlsSyntaxRules.getInstance ());
    	
    	BusinessView obj = new BusinessView(getContext(), id);
    	
    	obj.setEnableStatus(true);
    	obj.setLabel("BV task " + t.getDuName());
    	
    	Vector<String> nodes = new Vector<String>();
    	obj.setNodes(nodes);
    	
    	Vector<TaskId> tasks = new Vector<TaskId>();
    	tasks.add(TaskId.createWithName(t.getDuName(), defaultVersion, t.mu.getDuName(), template));
    	obj.setObjects(tasks);
    	
    	Lines l = new Lines();    	
    	l.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?> <BusinessView xmlns=\"http://www.orsyp.com/univiewer/hive\" xmlns:hiveInternals=\"http://www.orsyp.com/univiewer/hiveInternals\" hiveInternals:__hiveVersion__=\"0\"> <Canvas zoomLevel=\"0.0\" viewPortXLocation=\"0\" viewPortYLocation=\"0\" viewPortWidth=\"1569\" viewPortHeight=\"844\" alignToGridEnabled=\"true\" alignToGridVisible=\"false\" alignToGridSize=\"MEDIUM\" hiveInternals:__hiveAttributesTypes__=\"zoomLevel=java.lang.Double;viewPortXLocation=java.lang.Integer;viewPortYLocation=java.lang.Integer;viewPortWidth=java.lang.Integer;viewPortHeight=java.lang.Integer;alignToGridEnabled=java.lang.Boolean;alignToGridVisible=java.lang.Boolean;alignToGridSize=java.lang.String\" /> <UprocsDesign /> <SessionsDesign /> <ResourcesDesign /><TasksDesign /><Labels /><SessionLinks /><AggregatedLinks /><UprocDependencyLinks /><LinkVisibility><LinkVisibilitySetting name=\"InsideSessionDependencyLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\" /> <LinkVisibilitySetting name=\"OutsideSessionDependencyLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\" /><LinkVisibilitySetting name=\"SessionLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\" /><LinkVisibilitySetting name=\"ResourceConditionLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\" /></LinkVisibility></BusinessView>");
    	obj.setData(l);
    	
    	obj.setOffset(0, 1, 2, 3, 4, 5);
    	obj.setType(BusinessView.TYPE_CODE_BUSINESS_VIEW); 
        
        obj.setImpl( new OwlsBusinessViewImpl() );
        
        
        Vector<BvmCriteria> criterias = new Vector<BvmCriteria>();
    	criterias.add( new BvmCriteria("", "", "", "", "", "", "", "", "", "I", "Aborted", new Color(216,0,0)) );
    	criterias.add( new BvmCriteria("", "", "", "", "", "", "", "", "", "R", "Refused", new Color(255,100,100)) );
    	criterias.add( new BvmCriteria("", "", "", "", "", "", "", "", "", "O", "Time overrun", new Color(255,149,32)) );
    	criterias.add( new BvmCriteria("", "", "", "", "", "", "", "", "", "E", "Running", new Color(94,174,255)) );
    	criterias.add( new BvmCriteria("", "", "", "", "", "", "", "", "", "W", "Event wait", new Color(255,255,128)) );
    	criterias.add( new BvmCriteria("", "", "", "", "", "", "", "", "", "L", "Launch wait", new Color(152,251,152)) );
    	criterias.add( new BvmCriteria("", "", "", "", "", "", "", "", "", "T", "Completed", new Color(0,200,0)) );
    	obj.setCriterion(criterias);
        
        obj.create();
    	
		
		/*
		BusinessView bv = new BusinessView(getContext(), t.getDuName());
    	bv.setImpl(new OwlsBusinessViewImpl());    	    	
    	BusinessViewId id = new BusinessViewId(t.getDuName());
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		bv.setIdentifier(id);

		Lines l = new Lines();
		l.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?><BusinessView xmlns=\"http://www.orsyp.com/univiewer/hive\" xmlns:hiveInternals=\"http://www.orsyp.com/univiewer/hiveInternals\" hiveInternals:__hiveVersion__=\"0\">  <Canvas zoomLevel=\"0.0\" viewPortXLocation=\"0\" viewPortYLocation=\"0\" viewPortWidth=\"1583\" viewPortHeight=\"772\" alignToGridEnabled=\"true\" alignToGridVisible=\"false\" alignToGridSize=\"MEDIUM\" hiveInternals:__hiveAttributesTypes__=\"zoomLevel=java.lang.Double;viewPortXLocation=java.lang.Integer;viewPortYLocation=java.lang.Integer;viewPortWidth=java.lang.Integer;viewPortHeight=java.lang.Integer;alignToGridEnabled=java.lang.Boolean;alignToGridVisible=java.lang.Boolean;alignToGridSize=java.lang.String\"/>  <UprocsDesign/>  <SessionsDesign/>  <ResourcesDesign/>  <TasksDesign>    <TaskDesign groupLocationX=\"24.0\" groupLocationY=\"24.0\" taskName=\"BADGER_HSKP_JOB1\" templateName=\"BADGER\" sessionName=\"BADGER_HSKP_JOB1\" sessionVersion=\"000\" uprocName=\"H_BADGER_HSKP_JOB1\" uprocVersion=\"000\" objectEnvironmentNode=\"itlpmps01\" objectEnvironmentCompany=\"UNIV60\" objectEnvironmentArea=\"X\" backgroundColorRed=\"208\" backgroundColorGreen=\"208\" backgroundColorBlue=\"208\" backgroundColorAlpha=\"128\" reorganizationTypeId=\"ReorganizeStandardCommand\" sessionNeedsReorganization=\"false\" groupReduced=\"false\" hiveInternals:__hiveAttributesTypes__=\"groupLocationX=java.lang.Double;groupLocationY=java.lang.Double;taskName=java.lang.String;templateName=java.lang.String;sessionName=java.lang.String;sessionVersion=java.lang.String;uprocName=java.lang.String;uprocVersion=java.lang.String;objectEnvironmentNode=java.lang.String;objectEnvironmentCompany=java.lang.String;objectEnvironmentArea=java.lang.String;backgroundColorRed=java.lang.Integer;backgroundColorGreen=java.lang.Integer;backgroundColorBlue=java.lang.Integer;backgroundColorAlpha=java.lang.Integer;reorganizationTypeId=java.lang.String;sessionNeedsReorganization=java.lang.Boolean;groupReduced=java.lang.Boolean\">      <TaskSessionUprocDesign taskSessionUprocName=\"H_BADGER_HSKP_JOB1\" taskSessionMUName=\"\" itemLocationX=\"48.0\" itemLocationY=\"84.0\" hiveInternals:__hiveAttributesTypes__=\"taskSessionUprocName=java.lang.String;taskSessionMUName=java.lang.String;itemLocationX=java.lang.Double;itemLocationY=java.lang.Double\"/>      <TaskSessionUprocDesign taskSessionUprocName=\"DEFRAG0200\" taskSessionMUName=\"\" itemLocationX=\"204.0\" itemLocationY=\"168.0\" hiveInternals:__hiveAttributesTypes__=\"taskSessionUprocName=java.lang.String;taskSessionMUName=java.lang.String;itemLocationX=java.lang.Double;itemLocationY=java.lang.Double\"/>      <TaskSessionUprocDesign taskSessionUprocName=\"LOGCYCLER\" taskSessionMUName=\"\" itemLocationX=\"204.0\" itemLocationY=\"252.0\" hiveInternals:__hiveAttributesTypes__=\"taskSessionUprocName=java.lang.String;taskSessionMUName=java.lang.String;itemLocationX=java.lang.Double;itemLocationY=java.lang.Double\"/>      <TaskSessionUprocDesign taskSessionUprocName=\"COPYLOG\" taskSessionMUName=\"DEF_MU\" itemLocationX=\"360.0\" itemLocationY=\"336.0\" hiveInternals:__hiveAttributesTypes__=\"taskSessionUprocName=java.lang.String;taskSessionMUName=java.lang.String;itemLocationX=java.lang.Double;itemLocationY=java.lang.Double\"/>      <TaskSessionUprocDesign taskSessionUprocName=\"LOG0100\" taskSessionMUName=\"BADGER\" itemLocationX=\"516.0\" itemLocationY=\"420.0\" hiveInternals:__hiveAttributesTypes__=\"taskSessionUprocName=java.lang.String;taskSessionMUName=java.lang.String;itemLocationX=java.lang.Double;itemLocationY=java.lang.Double\"/>      <TaskSessionUprocDesign taskSessionUprocName=\"IISBKUP\" taskSessionMUName=\"DEF_MU\" itemLocationX=\"204.0\" itemLocationY=\"420.0\" hiveInternals:__hiveAttributesTypes__=\"taskSessionUprocName=java.lang.String;taskSessionMUName=java.lang.String;itemLocationX=java.lang.Double;itemLocationY=java.lang.Double\"/>      <TaskSessionUprocDesign taskSessionUprocName=\"T_BADGER_HSKP_JOB1\" taskSessionMUName=\"\" itemLocationX=\"204.0\" itemLocationY=\"504.0\" hiveInternals:__hiveAttributesTypes__=\"taskSessionUprocName=java.lang.String;taskSessionMUName=java.lang.String;itemLocationX=java.lang.Double;itemLocationY=java.lang.Double\"/>    </TaskDesign>  </TasksDesign>  <Labels/>  <SessionLinks>    <Link sourceXLocation=\"48.0\" sourceYLocation=\"84.0\" targetXLocation=\"204.0\" targetYLocation=\"168.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\"/>    <Link sourceXLocation=\"48.0\" sourceYLocation=\"84.0\" targetXLocation=\"204.0\" targetYLocation=\"252.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\"/>    <Link sourceXLocation=\"204.0\" sourceYLocation=\"252.0\" targetXLocation=\"360.0\" targetYLocation=\"336.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\"/>    <Link sourceXLocation=\"360.0\" sourceYLocation=\"336.0\" targetXLocation=\"516.0\" targetYLocation=\"420.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\"/>    <Link sourceXLocation=\"48.0\" sourceYLocation=\"84.0\" targetXLocation=\"204.0\" targetYLocation=\"420.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\"/>    <Link sourceXLocation=\"48.0\" sourceYLocation=\"84.0\" targetXLocation=\"204.0\" targetYLocation=\"504.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\"/>  </SessionLinks>  <AggregatedLinks/>  <UprocDependencyLinks>    <Link sourceXLocation=\"204.0\" sourceYLocation=\"420.0\" targetXLocation=\"204.0\" targetYLocation=\"504.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\">      <UprocDependencyLink conditionNumber=\"5\" hiveInternals:__hiveAttributesTypes__=\"conditionNumber=java.lang.Integer\"/>    </Link>    <Link sourceXLocation=\"204.0\" sourceYLocation=\"252.0\" targetXLocation=\"204.0\" targetYLocation=\"504.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\">      <UprocDependencyLink conditionNumber=\"2\" hiveInternals:__hiveAttributesTypes__=\"conditionNumber=java.lang.Integer\"/>    </Link>    <Link sourceXLocation=\"360.0\" sourceYLocation=\"336.0\" targetXLocation=\"204.0\" targetYLocation=\"504.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\">      <UprocDependencyLink conditionNumber=\"4\" hiveInternals:__hiveAttributesTypes__=\"conditionNumber=java.lang.Integer\"/>    </Link>    <Link sourceXLocation=\"516.0\" sourceYLocation=\"420.0\" targetXLocation=\"204.0\" targetYLocation=\"504.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\">      <UprocDependencyLink conditionNumber=\"3\" hiveInternals:__hiveAttributesTypes__=\"conditionNumber=java.lang.Integer\"/>    </Link>    <Link sourceXLocation=\"204.0\" sourceYLocation=\"168.0\" targetXLocation=\"204.0\" targetYLocation=\"504.0\" rendererId=\"straightLine\" hiveInternals:__hiveAttributesTypes__=\"sourceXLocation=java.lang.Double;sourceYLocation=java.lang.Double;targetXLocation=java.lang.Double;targetYLocation=java.lang.Double;rendererId=java.lang.String\">      <UprocDependencyLink conditionNumber=\"1\" hiveInternals:__hiveAttributesTypes__=\"conditionNumber=java.lang.Integer\"/>    </Link>  </UprocDependencyLinks>  <LinkVisibility>    <LinkVisibilitySetting name=\"InsideSessionDependencyLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\"/>    <LinkVisibilitySetting name=\"OutsideSessionDependencyLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\"/>    <LinkVisibilitySetting name=\"SessionLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\"/>    <LinkVisibilitySetting name=\"ResourceConditionLink\" visible=\"true\" hiveInternals:__hiveAttributesTypes__=\"name=java.lang.String;visible=java.lang.Boolean\"/>  </LinkVisibility></BusinessView>");
		bv.setData(l);
		Vector<TaskId> v = new Vector<TaskId>();
		v.add(TaskId.createWithName(t.getDuName(), defaultVersion, t.mu.getDuName(), template));
		bv.setObjects(v);
		bv.setType(BusinessView.TYPE_CODE_BUSINESS_VIEW);		
		bv.create();
		*/
	}

	public void duplicateUproc(String srcUproc, String targetName) throws Exception{
		Uproc uproc = getUProc(srcUproc);		
        UprocId newDuplicatedUprocId = new UprocId(targetName, defaultVersion);
        newDuplicatedUprocId.setId(targetName);  
        
        uproc.setImpl(new OwlsUprocImpl());
        uproc.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
        uproc.duplicate(newDuplicatedUprocId, uproc.getLabel());		
	}

	
	public void createTaskFromUniJobData(String taskName, Job job, String node, DuMu mu) throws Exception{
		Context c = getContext(node);
		Task t = TaskConverter.convert(job, taskName, mu.getDuName(), c);
		t.create();
	}

	public void createUprocFromUniJobData(String uprName, Job job, String node) throws Exception{
		Context c = getContext(node);
		Uproc u = UprocConverter.convert(job, uprName, c);
		u.create();
	}
	
	public void createUserFromUniJob(String user, String pw, String node) throws Exception{
		Context c = getContext(node);
		UserConverter.createUser(user, pw, c);		
	}
}
