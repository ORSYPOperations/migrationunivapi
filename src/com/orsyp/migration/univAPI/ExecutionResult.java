package com.orsyp.migration.univAPI;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Wrapper class for testing uproc/task executions.
 * @author rbr
 *
 */
public class ExecutionResult {

	public String uproc;
	public String session;
	public String mu;
	public String status;
	public String procDate;
	public Date beginDate;
	public Date endDate;
	
	public String getTime() {
		return new SimpleDateFormat("HH:mm").format(beginDate);
	}
		
	public String getDay() {
		return new SimpleDateFormat("dd/MM/yyyy").format(beginDate);
	}
	
	public String getDuration() {
		if (endDate!=null) {
			long d = (endDate.getTime() - beginDate.getTime()) /60000;
			if (d==0)
				return "<1";
			return ""+ d;
		}
		else
			return null;
	}
	
}
